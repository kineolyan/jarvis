(ns jarvis.scheduler-test
  (:require [clojure.test :as t :refer [deftest testing is]]
            [missionary.core :as m]
            [jarvis.scheduler :as s]))

(deftest interval->millis
  (is (= 3000 (s/interval->millis {:sec 3})))
  (is (= (* 2 60 1000)
         (s/interval->millis {:min 2})))
  (is (= (+ 27000 (* 1 60 60 1000))
         (s/interval->millis {:hour 1 :sec 27}))))

(defn t
  [& args]
  (s/interval->millis (apply hash-map args)))

(comment
  (t :sec 3))

(deftest get-past-operations
  (let [state {:operations {:a {:last-execution (t :sec 10)
                                :status :idle
                                :interval {:sec 7}}
                            :b {:last-execution (t :sec 20)
                                :status :idle
                                :interval {:sec 10}}
                            :c {:last-execution 0
                                :interval {:sec 1}
                                :status :executing}
                            :d {:last-execution 0
                                :interval {:sec 1}
                                :status :pending}}}
        schedules (s/get-operation-schedules state)
        do-test #(->> (s/get-past-operations schedules %)
                      (map :operation)
                      (set))]
    (testing "before all operations"
      (is (= #{}
             (do-test 0))))
    (testing "after execution and before its repetition"
      (is (= #{}
             (do-test (t :sec 11)))))
    (testing "between two operations"
      (is (= #{:a}
             (do-test (t :sec 18))))
      (is (= #{:a}
             (do-test (t :sec 25)))))
    (testing "after"
      (is (= #{:a :b}
             (do-test (t :sec 40)))))))

(deftest create-action
  (testing "with no-arg function"
    (let [a (atom 0)
          action (s/-create-action #(swap! a inc))
          result (m/? action)]
      (is (= 1 result))
      (is (= 1 @a))))

  (testing "with standard missionary task"
    (let [a (atom 8)
          op (s/-create-action (m/sp (swap! a dec)))
          result (m/? op)]
      (is (= 7 result))
      (is (= 7 @a)))))

(deftest create-scheduling-action
  (let [schedules [{:operation :a :at 1000}
                   {:operation :b :at 2000}]
        now 1500]
    (testing "simple execution"
      (let [state (atom [])
            a (s/create-scheduling-action schedules
                                          now
                                          #(swap! state conj %))
            {:keys [trigger action]} a]
        (trigger)
        (m/? action)
        (is (= @state [:a :b]))))

    (testing "waiting on the trigger"
      (let [state (atom [])
            a (s/create-scheduling-action schedules
                                          now
                                          #(swap! state conj [:executing %]))
            {:keys [trigger action]} a]
        (action (fn [_r] (swap! state conj [:completed]))
                (fn [e] (swap! state conj [:error e])))
        (is (= @state []))
        (trigger)
        (loop [i 0]
          (cond
            (= i 20) ;; too much waiting
            (throw (AssertionError. "Incomplete action"))
            (not= @state [[:executing :a] [:executing :b] [:completed]])
            (do (Thread/sleep 50)
                (recur (inc i)))))))

    (testing "cancelling before trigger"
      (let [state (atom [])
            a (s/create-scheduling-action schedules
                                          now
                                          #(swap! state conj [:executing %]))
            {:keys [action]} a
            cancel (action (fn [r] (swap! state conj [:done r]))
                           (fn [e] (swap! state conj [:error e])))]
        (Thread/sleep 100)
        (is (= @state [])) ; sanity check
        (cancel)
        (is (= [:error] (map first @state)))))))

; disabling this flaky test for now
#_(deftest integration
    (let [state (atom {})
          scheduler (s/create)]
      (try
        (s/start! scheduler)
        (s/schedule! scheduler {:schedule-id :a
                                :interval {:ms 40}
                                :action #(swap! state update :a (fnil inc 0))})
        (s/schedule! scheduler {:schedule-id :b
                                :interval {:hour 1}
                                :action #(swap! state update :b (fnil inc 0))})
        (Thread/sleep 100) ; wait a bit to let the two tasks run
        (dotimes [_ 3]
          (s/execute-now! scheduler :b)
          (s/execute-now! scheduler :a)
          (Thread/sleep 10))
        (Thread/sleep 100)
        (doseq [op [:a :b]]
          (s/deschedule! scheduler op))
      ; :b should be executed once by `schedule!` and 3 times manually
        (is (<= 4 (:b @state)))
      ; :a should have run once by `schedule!`, 2+ times during the two sleep
      ; and 3 times manually
        (is (<= (+ 1 2 3) (:a @state)))
        (finally (s/stop! scheduler)))))

(comment
  (future (Thread/sleep 100) (println "hello")))
