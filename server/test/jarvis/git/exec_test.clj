(ns jarvis.git.exec-test
  (:require [jarvis.git.exec :as m]
            [clojure.test :refer [deftest is are]]))

(deftest parse-branch-status
  (is (= {:branch "correct-server-build"
          :sha "4c46fda"
          :checked false
          :gone false}
         (m/parse-branch-status
          "  correct-server-build    4c46fda [origin/correct-server-build] Correct the lein command.")))
  (is (= {:branch "main"
          :sha "ad9f7c3"
          :checked false
          :gone false}
         (m/parse-branch-status
          "  main                    ad9f7c3 [origin/main: behind 2] Merge branch 'improve-trigger' into 'main'")))
  (is (= {:branch "migrate-to-deps"
          :sha "cc17f8a"
          :checked false
          :gone false}
         (m/parse-branch-status
          "  migrate-to-deps         cc17f8a Merge branch 'daemon-mode-for-server' into 'main'")))
  (is (= {:branch "notify-deleted-branches"
          :sha "f3e0331"
          :checked true
          :gone false}
         (m/parse-branch-status
          "* notify-deleted-branches f3e0331 Merge branch 'correct-server-build' into 'main'")))
  (is (= {:branch "explore-design"
          :sha "fd348d3"
          :checked false
          :gone true}
         (m/parse-branch-status
          "  explore-design          fd348d3 [origin/explore-design: gone] Correct exec to return a comment.")))
  ; Branch with `/` within
  (is (= {:branch "project/tck"
          :sha "205e8420e6"
          :checked true
          :gone false}
         (m/parse-branch-status
          "* project/tck     205e8420e6 [origin/project/tck] Merge remote-tracking branch 'origin/main'")))
  ; Currently detached from any branch
  (is (nil?
       (m/parse-branch-status
        "* (HEAD detached at origin/main) 7c6ab23 Merge branch 'br' into 'main'")))

  (is (= {:branch "feature/5.11/use-formatter-maven"
          :sha "a29ba7ba40"
          :checked false
          :gone false}
         (m/parse-branch-status
          "  feature/5.11/use-formatter-maven                  a29ba7ba40 [origin/feature/5.11/use-formatter-maven] A batch of formatting.")))

  (is (= {:branch "something_in/snake_case"
          :sha "47503444742"
          :checked false
          :gone false}
         (m/parse-branch-status
          "  something_in/snake_case      47503444742 [origin/something_in/snake_case] Commit message")))

  (is (= {:branch "local/name"
          :sha "42047250342"
          :checked false
          :gone false}
         (m/parse-branch-status
          "  local/name    42047250342 [upstream/remote/name] Commit")))

  (is (= {:branch "feature/5.12/improve-cross"
          :sha "3c9dc00b609"
          :checked true
          :gone true}
         (m/parse-branch-status "+ feature/5.12/improve-cross            3c9dc00b609 (/workspaces/5.11/std) [origin/feature/5.12/improve-cross: gone] Correct script."))))

(deftest delete-branch-candidate?
  (are [status expected] (= (m/delete-branch-candidate? status) expected)
    {:gone true :checked false} true
    {:gone true :checked true} false
    {:gone false :checked false} false
    {:gone false :checked true} false))

(deftest uncleanable-branch-candidate?
  (are [status expected] (= (m/uncleanable-branch-candidate? status) expected)
    {:gone true :checked false} false
    {:gone true :checked true} true
    {:gone false :checked false} false
    {:gone false :checked true} false))

