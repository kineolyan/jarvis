(ns jarvis.git.monitor.task-test
  (:require [clojure.test :refer [deftest is]]
            [jarvis.git.monitor.task :as m]))

(deftest list-projects-to-add
  (let [git-db [{:git "a"}
                {:git "b"}
                {:git "e"}]
        projects {"b" 1 "c" 2 "d" 3}
        ks (m/list-projects-to-add git-db projects)]
    (is (= #{"a" "e"} ks))))

(deftest list-projects-to-update
  (let [git-db  [{:git "a"}
                 {:git "b"}
                 {:git "d"}]
        projects {"b" 1 "c" 2 "d" 3}
        ks (m/list-projects-to-update git-db projects)]
    (is (= #{"b" "d"} ks))))

(deftest list-projects-to-delete
  (let [git-db [{:git "a"}
                {:git "b"}]
        projects {"b" 1 "c" 2 "d" 3}
        ks (m/list-projects-to-delete git-db projects)]
    (is (= #{"c" "d"} ks))))

(deftest classify-projects
  (let [git-db [{:git "a"}
                {:git "c"}]
        projects {"b" 2 "c" 3}
        result (m/classify-projects git-db projects)]
    (is (= {:added #{"a"}
            :updated #{"c"}
            :deleted #{"b"}}
           result))))
