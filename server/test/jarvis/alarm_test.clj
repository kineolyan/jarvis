(ns jarvis.alarm-test
  (:require [clojure.test :as t :refer [deftest testing is]]
            [jarvis.alarm :as m])
  (:import [java.time LocalTime]))

(deftest time-to-integer-value
  (testing "with only seconds"
    (is (= 48 (m/time->epoch-sec (LocalTime/of 0 48)))))
  (testing "with only hours"
    (is (= 120 (m/time->epoch-sec (LocalTime/of 2 0)))))
  (testing "complete test"
    (is (= 187 (m/time->epoch-sec (LocalTime/of 3 7))))))

(deftest create-alarm-notif
  (testing "after the target time"
    (let [target (LocalTime/of 10 20)
          current (LocalTime/of 12 0)]
      (is (nil?
           (m/create-alarm-notif-args
            target
            current
            {:title "t"})))))

  (testing "exactly at the target time"
    (let [target (LocalTime/of 10 20)]
      (is (=
           {:title "t"
            :message "Wake up and go for it"
            :level :critical
            :duration 30}
           (m/create-alarm-notif-args
            target
            target
            {:title "t"})))))

  (testing "before the target time"
    (testing "long before"
      (let [target (LocalTime/of 10 20)
            current (LocalTime/of 8 0)]
        (is (nil?
             (m/create-alarm-notif-args
              target
              current
              {:title "t"})))))
    (testing "reasonably before for notif"
      (let [target (LocalTime/of 10 40)
            current (LocalTime/of 10 20)]
        (is (=
             {:title "t"
              :message "Time to consider it"
              :level :normal
              :duration 10}
             (m/create-alarm-notif-args
              target
              current
              {:title "t"})))))))
