(ns jarvis.notifications-test
  (:require
   [clojure.test :as t :refer [deftest is testing]]
   [jarvis.notifications :as m]))

(defn with-service [f]
  (let [service (m/init nil)]
    (m/start! service)
    (try
      (f service)
      (finally (m/stop! service)))))

(defn trim-messages
  "Trims messages to retain only information that can be tested"
  [messages]
  (map #(select-keys % [:id :content]) messages))

(defn inspect-messages
  [service]
  (:messages @service))

(defn has-message
  [messages message]
  (is (some #{message} (trim-messages messages))))

(defn has-no-message
  [messages message]
  (is (not-any? #{message} (trim-messages messages))))

(defn create-messages!
  [service ids]
  (doseq [i ids]
    (m/notify! service [:test i] i)))

(defn let-time-change!
  []
  (Thread/sleep 1))

(defn create-read-messages!
  "Create read messages in the services with id as `[:test <id>]` and content as `<id>`."
  [service ids]
  (create-messages! service ids)
  (let-time-change!)
  (m/find-new-notifications! service)
  (let-time-change!))

(deftest notify!
  (testing "add a message with a new id"
    (with-service
      (fn [service]
        (m/notify! service :id-1 "abc")
        (has-message (inspect-messages service) {:id :id-1 :content "abc"})))))

(deftest discard!
  (testing "for unread message"
    (with-service
      (fn [service]
        (m/notify! service :msg "abc")
        (is (= 1 (m/count-new-notifications service)))
        (has-message (inspect-messages service) {:id :msg :content "abc"})
        (m/discard! service :msg)
        (is (zero? (m/count-new-notifications service)))
        (has-no-message (inspect-messages service) {:id :msg :content "abc"}))))
  (testing "for read message"
    (with-service
      (fn [service]
        (create-read-messages! service [:a :b])
        (m/discard! service [:test :b])
        (has-no-message (inspect-messages service) {:id [:test :b] :content :b})
        (is (zero? (m/count-new-notifications service))))))
  (testing "for unknown message"
    (with-service
      (fn [service]
        (m/discard! service :unknown)
        (is (empty? (m/get-all-notifications service)))))))

(deftest count-new-notifications
  (testing "with only new messages"
    (with-service
      (fn [service] (create-messages! service (range 1 4))
        (is (= 3 (m/count-new-notifications service))))))

  (testing "with only read messages"
    (with-service
      (fn [service]
        (create-read-messages! service (range 1 4))
        (is (zero? (m/count-new-notifications service))))))

  (testing "with some read messages and new ones"
    (with-service
      (fn [service]
        (create-read-messages! service (range 1 4))
        (create-messages! service (range 10 12))
        (is (= 2 (m/count-new-notifications service)))))))

(deftest find-new-notifications!
  (testing "with only new messages"
    (with-service
      (fn [service]
        (let [ids (range 1 4)
              _ (create-messages! service (range 1 4))
              messages (m/find-new-notifications! service)]
          (is (= 3 (count messages)))
          (doseq [i ids]
            (has-message messages {:id [:test i] :content i}))))))

  (testing "with only read messages"
    (with-service
      (fn [service]
        (let [_ (create-read-messages! service (range 1 4))
              messages (m/find-new-notifications! service)]
          (is (empty? messages))))))

  (testing "with some read messages and new ones"
    (with-service
      (fn [service]
        (let [_ (create-read-messages! service (range 1 4))
              ids (range 10 13)
              _ (create-messages! service ids)
              messages (m/find-new-notifications! service)]
          (is (= 3 (count messages)))
          (doseq [i ids]
            (has-message messages {:id [:test i] :content i})))))))

(deftest get-all-notifications
  (with-service
    (fn [service]
      (create-read-messages! service [1])
      (create-messages! service [10])
      (let [messages (m/get-all-notifications service)]
        (doseq [i [1 10]]
          (has-message messages {:id [:test i] :content i}))))))

(deftest update-existing-message
  (testing "before read"
    (with-service
      (fn [service]
        (m/notify! service :test-id 1)
        (let-time-change!)
        (m/notify! service :test-id 2)
        (testing "all messages"
          (let [messages (m/get-all-notifications service)]
            (has-message messages {:id :test-id :content 2})
            (is (= 1 (count messages)))))
        (testing "unread messages"
          (let [messages (m/find-new-notifications! service)]
            (has-message messages {:id :test-id :content 2})
            (is (= 1 (count messages))))))))
  (testing "after read"
    (with-service
      (fn [service]
        (m/notify! service :test-id 1)
        (let-time-change!)
        (m/find-new-notifications! service)
        (let-time-change!)
        (m/notify! service :test-id 2)
        (let-time-change!)
        (testing "all messages"
          (let [messages (m/get-all-notifications service)]
            (has-message messages {:id :test-id :content 2})
            (is (= 1 (count messages)))))
        (testing "unread messages"
          (let [messages (m/find-new-notifications! service)]
            (has-message messages {:id :test-id :content 2})
            (is (= 1 (count messages)))))))))

(deftest purge
  (with-service
    (fn [service]
      (create-read-messages! service (range 1 5))
      (create-messages! service (range 10 14))
      (is (seq (inspect-messages service)))
      (m/purge! service)
      (is (empty? (inspect-messages service))))))
