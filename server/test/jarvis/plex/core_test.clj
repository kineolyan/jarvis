(ns jarvis.plex.core-test
  (:require [clojure.test :refer [deftest is testing]]
            [jarvis.plex.core :as c]))

(deftest episode-info
  (testing "pattern s<xx>-e<yy>"
    (is (= {::c/season 1 ::c/episode 23}
           (c/guess-episode-info "TBBT-s1e23.avi")))
    (is (= {::c/season 12 ::c/episode 3}
           (c/guess-episode-info "TBBT-s12e3.mkv")))
    (testing "with case"
      (is (= {::c/season 1 ::c/episode 23}
             (c/guess-episode-info "TBBT-S1E23.avi")))))

  (testing "pattern <ss>x<ee>"
    (testing "with dash"
      (is (= {::c/season 4 ::c/episode 2}
             (c/guess-episode-info "TBBT-4x02-720p.avi")))
      (testing "with underscores"
        (is (= {::c/season 4 ::c/episode 2}
               (c/guess-episode-info "TBBT_4x02_720p.avi")))))
    (testing "with spaces"
      (is (= {::c/season 4 ::c/episode 2}
             (c/guess-episode-info "TBBT 4x02 720p.avi"))))
    (testing "with dots"
      (is (= {::c/season 4 ::c/episode 2}
             (c/guess-episode-info "TBBT.4x02.720p.avi"))))
    (testing "with mix"
      (is (= {::c/season 4 ::c/episode 2}
             (c/guess-episode-info "TBBT-4x02.720p.avi"))))
    (testing "with only this information"
      (is (= {::c/season 4 ::c/episode 2}
             (c/guess-episode-info "4x02.avi"))))
    (testing "with path details"
      (is (= {::c/season 4 ::c/episode 2}
             (c/guess-episode-info "/a/c/4x02.avi"))))))
