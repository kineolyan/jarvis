(ns jarvis.keyvault-test
  (:require [clojure.test :as t :refer [deftest testing is]]
            [jarvis.keyvault :as m]))

(def ^:dynamic *keyvault* nil)
(defn keyvault-service [f]
  (let [service (m/init nil)]
    (m/start! service)
    (binding [*keyvault* service] (f))
    (m/stop! service)))

(defn with-entry
  [k v]
  (= v (m/find-entry *keyvault* k)))

(defn without-entry
  [k]
  (nil? (m/find-entry *keyvault* k)))

(t/use-fixtures :each keyvault-service)

(deftest add-entry
  (testing "add new key"
    (m/add-entry! *keyvault* "ABC" "abc")
    (is (with-entry "ABC" "abc")))
  (testing "add several keys"
    (m/add-entry! *keyvault* "A" "a")
    (m/add-entry! *keyvault* "B" "b")
    (is (with-entry "A" "a"))
    (is (with-entry "B" "b")))
  (testing "add existing entry"
    (doseq [i (range 1 4)]
      (m/add-entry! *keyvault* "XYZ" (str i)))
    (is (with-entry "XYZ" "3"))
    ;; Check that there is only one entry with the key
    (let [entries (frequencies (m/list-entries *keyvault*))]
      (is (= 1 (get entries "XYZ"))))))

(deftest remove-entry
  (testing "remove existing key"
    (m/add-entry! *keyvault* "X" "1")
    (m/add-entry! *keyvault* "Y" "2")
    (m/remove-entry! *keyvault* "X")
    (is (with-entry "Y" "2"))
    (is (without-entry "X"))))

(deftest list-entries
  (testing "list keys"
    (m/add-entry! *keyvault* "I" "1")
    (m/add-entry! *keyvault* "II" "2")
    (is (= ["I" "II"]
           (m/list-entries *keyvault*)))))

