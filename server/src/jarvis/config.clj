(ns jarvis.config
  (:require [clojure.edn :as edn]
            [babashka.fs :as fs]))

(defonce config-registry (atom {}))

(defn register-config
  "Registers a config file for a given key."
  [k file]
  (let [absolute-path (fs/absolutize file)]
    (swap! config-registry assoc k absolute-path)))

(defn get-config-file
  "Safely gets the file name storing a configuration."
  [k]
  (let [filename (get @config-registry k)]
    (when-not filename (throw (IllegalArgumentException. (str "No config for " k))))
    filename))

(defn read-config
  "Reads the config for a given key k.
   If the config does not exist, it returns nil."
  [k]
  (let [file (get-config-file k)]
    (when (fs/exists? file)
      (-> file fs/file slurp edn/read-string))))

(defn write-config
  "Writes the passed configuration for the key k."
  [k content]
  (let [config-path (get-config-file k)
        parent-dir (fs/parent config-path)]
    (when-not (fs/exists? parent-dir)
      (fs/create-dirs parent-dir))
    (spit (fs/file config-path) (pr-str content))))

(defn update-config
  [k f]
  (->> (read-config k)
       (f)
       (write-config k)))

(comment
  (def file (fs/path "/tmp/super/config/dir/jarvis-git.edn"))
  config-registry
  (register-config 'git file)

  (write-config 'git {'path "here" 'action 'greeting})

  ('path (read-config 'git)))
