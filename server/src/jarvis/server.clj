(ns jarvis.server
  (:require [babashka.fs :as fs]
            [clojure.core.async :as async]
            [clojure.string :as str]
            [clojure.pprint :as pp]
            [jarvis.cmd.command :as cmd]
            [taoensso.timbre :as timbre]
            [kineolyan.netfuse :as netfuse]
            [jarvis.com.server :as server]
            [jarvis.git.exec :as git-exec]
            [jarvis.git.monitor.core :as git-core]
            [jarvis.git.monitor.ctl :as git-ctl]
            [jarvis.git.monitor.task :as git-task]
            [jarvis.keyvault :as kv]
            [jarvis.notifications :as notifs]
            [jarvis.plex.core :as plex]
            [jarvis.scheduler :as s]
            [jarvis.ssh :as ssh]
            [jarvis.netlify.kvs :as kvs]
            [jarvis.alarm :as alarm])
  (:import [java.time LocalTime]
           [java.security MessageDigest]))

(defmacro create-constant
  [n f]
  (let [value (eval f)]
    `(def ~n ~value)))

(create-constant
 app-sha1
 (System/getProperty "jarvis.sha1" "<unknown>"))
(create-constant
 app-version
 (System/getProperty "jarvis.version" "<undefined>"))

(defn md5sum
  "Computes the MD5 sum for any object."
  [input]
  (let [md (MessageDigest/getInstance "MD5")
        data (if (string? input) input (pr-str input))
        bytes (.digest md (.getBytes data))
        hex (clojure.string/join (map #(format "%02x" %1) bytes))]
    hex))

(comment
  (md5sum "ab")
  (md5sum [1 {:a 'bc}]))

(defn -hash?
  [id]
  (str/starts-with? id "@"))

(defn -find-by-hash
  [hash-fn xs hash]
  (let [p #(= hash (hash-fn %))
        [y & ys] (filter p xs)]
    (cond
      (nil? y) nil
      (seq? ys) (throw (ex-info "Too many items"
                                {:items xs
                                 :needle hash
                                 :fn hash-fn}))
      :else y)))

;;; -- Tools

(defn new-version?
  "Checks if the value represents a new version of this application."
  [value]
  (and (not= app-sha1 "<unknown>")
       (not= value app-sha1)))

(defn notify-new-version!
  "Creates a new notification about an application update."
  [{:keys [news] :as _app}]
  (notifs/notify! news [:app :new-version] "New version of Jarvis available. Don't forget to download it"))

(defn get-latest-version!
  "Gets the SHA1 of the latest version of the application, or `nil` if it cannot be retrieved."
  []
  (try
    (netfuse/run-with-fuse!
     git-exec/get-repository-head!
     {:url "https://gitlab.com/kineolyan/jarvis"})
    (catch Exception e
      (timbre/debugf e "Cannot retrieved the head SHA1")
      nil)))

; TODO migrate to shellby and missionary
(defn check-new-version-available!
  [app]
  (loop []
    (timbre/trace "Trying to retrieve the head SHA1")
    (if-let [remote-sha1 (get-latest-version!)]
      (do
        (timbre/tracef "Application HEAD SHA1 = %s" remote-sha1)
        (if (new-version? remote-sha1)
          (notify-new-version! app)
          (timbre/info "No new version of Jarvis ¯\\_('-')_/¯")))
      (do (timbre/tracef "No version. Retrying in a moment...")
          (Thread/sleep 1000)
          (recur)))))

;;; -- Actions

(defn print-git-project-status
  [{:keys [repository branch status mode]}]
  (println (str " - repository: " repository))
  (println (str "   branch: " branch))
  (println (str "   mode " mode))
  (println (str "   status: " status)))

(defn handle-git-status-cmd
  [{{git-state :git} :app
    {:keys [repository]} :capture}]
  (if-let [git-project (git-ctl/get-entry @(::git-ctl/config git-state) repository)]
    (with-out-str (print-git-project-status git-project))
    (str "No match for " repository)))

(defn handle-git-attach-cmd
  [{{git-state :git scheduler :git-scheduler} :app
    {:keys [repository branch]} :capture}]
  (git-ctl/attach! git-state repository branch)
  (git-task/trigger-update! scheduler)
  (str branch " attached to " repository))

(defn handle-git-detach-cmd
  [{{git-state :git scheduler :git-scheduler} :app
    {:keys [repository]} :capture}]
  (git-ctl/detach! git-state repository)
  (git-task/trigger-update! scheduler)
  (str "detached from " repository))

(defn handle-git-manage-cmd
  [{{git-state :git} :app
    {:keys [repository branch]} :capture}]
  (git-ctl/manage! git-state repository branch)
  (str "managing " repository " using branch " branch))

(defn handle-git-unmanage-cmd
  [{{git-state :git scheduler :git-scheduler} :app
    {:keys [repository]} :capture}]
  (git-ctl/unmanage! git-state repository)
  (git-task/trigger-update! scheduler)
  (str "stop managing " repository))

(defn handle-git-pin-cmd
  [{{git-state :git} :app
    {:keys [repository]} :capture}]
  (git-ctl/pin! git-state repository)
  (str "pinning " repository))

(defn handle-git-unpin-cmd
  [{{git-state :git scheduler :git-scheduler} :app
    {:keys [repository]} :capture}]
  (git-ctl/unpin! git-state repository)
  (git-task/trigger-update! scheduler repository)
  (str "freeing " repository))

(defn handle-git-enter-cmd
  [{{git-state :git} :app
    {:keys [repository branch]} :capture}]
  (git-ctl/enter! git-state repository branch)
  (str "entering branch " branch " to code :)"))

(defn handle-git-exit-cmd
  [{{git-state :git scheduler :git-scheduler} :app
    {:keys [repository]} :capture}]
  (git-ctl/exit! git-state repository)
  (git-task/trigger-update! scheduler repository)
  (str "Done with " repository ". Be free!"))

(defn handle-git-repair-cmd
  [{{git-state :git scheduler :git-scheduler} :app
    {:keys [repository]} :capture}]
  (git-ctl/repair! git-state repository)
  (git-task/trigger-update! scheduler repository)
  (str "trying to repair " repository))

(defn handle-git-sync-cmd
  [{{scheduler :git-scheduler} :app}]
  (git-task/trigger-update! scheduler)
  "syncing all repositories")

(defn handle-git-clean-branches
  [{{:keys [cwd]} :context}]
  (git-ctl/clean-deleted-branches! cwd))

(defn handle-git-check-branches
  [{{:keys [cwd]} :context}]
  (git-ctl/clean-deleted-branches! cwd :dry-run true))

(defn print-version
  []
  (println (format "Version: %s (git@%s)" app-version app-sha1)))

(defn print-service-status
  [name print-content]
  (println name)
  (println "-------")
  (print-content)
  (println))

(defn print-git-status
  [git]
  (print-service-status
   "Git"
   #(doseq [git-project @(::git-ctl/config git)]
      (print-git-project-status git-project))))

(defn print-scheduler-status
  [scheduler]
  (print-service-status
   "Scheduler"
   #(s/status scheduler :print true)))

(defn print-server-status
  [server]
  (print-service-status
   "Server"
   #(do (println "Port " (-> server :socket deref .getLocalPort))
        (println (str "Connections: " (-> server :connections deref count))))))

(defn print-status
  "Prints the status of the entire application"
  [{:keys [git scheduler server]}]
  (print-version)
  (print-server-status server)
  (print-git-status git)
  (print-scheduler-status scheduler))

(defn handle-status-cmd
  [{app :app}]
  (with-out-str (print-status app)))

(defn ensure-absolute-paths
  [cwd entries]
  (into []
        (comp
         (map #(if (fs/absolute? %) (fs/path %) (fs/path cwd %)))
         (map fs/expand-home)
         (map fs/normalize))
        entries))

(comment
  (ensure-absolute-paths "/tmp" ["../oliv/.gitconfig" "/home/oliv/.gitconfig"]))

(defn handle-plex-series-cmd
  [{:keys [app capture context]}]
  (let [{:keys [name file-or-directory]} capture
        payload {::plex/serie-name name
                 ::plex/files-or-directories (ensure-absolute-paths (:cwd context)
                                                                    [file-or-directory])}]
    (plex/add-serie (:plex app) payload)))

(defn -create-news-output
  [entries]
  (if (seq entries)
    (map #(format "%s\n   at %s"
                  (% :content)
                  (-> (% :time) java.time.Instant/ofEpochMilli str))
         entries)
    "No news for now"))

(defn handle-news-cmd
  [{:keys [app]}]
  (let [entries (notifs/find-new-notifications! (:news app))]
    (-create-news-output entries)))

(defn handle-news-all-cmd
  [{:keys [app]}]
  (let [entries (notifs/get-all-notifications (:news app))]
    (-create-news-output entries)))

(defn handle-news-count-cmd
  [{:keys [app]}]
  [:raw (notifs/count-new-notifications (:news app))])

(defn handle-news-purge-cmd
  [{:keys [app]}]
  (notifs/purge! (:news app))
  "No more news")

(defn handle-keyvault-list-cmd
  [{:keys [app]}]
  (if-let [ks (seq (kv/list-entries (app :keyvault)))]
    (concat
     ["Keys in the vault:"]
     (map #(format " - %s" %) ks))
    "No keys in the vault"))

(defn handle-keyvault-get-cmd
  [{:keys [app capture]}]
  (if-let [value (kv/find-entry (:keyvault app) (:key capture))]
    value
    (format "No value for <%s>" (:key capture))))

(defn handle-keyvault-set-cmd!
  [{:keys [app capture]}]
  (kv/add-entry! (:keyvault app) (:key capture) (:value capture))
  (format "Key <%s> added to the vault" (:key capture)))

(defn handle-keyvault-unset-cmd!
  [{:keys [app capture]}]
  (kv/remove-entry! (:keyvault app) (:key capture))
  (format "Key <%s> removed from the vault" (:key capture)))

(defn handle-ssh-status!
  [{:keys [app]}]
  (let [report (ssh/generate-report (:ssh-state app))]
    (with-out-str (pp/print-table report))))

(defn handle-ssh-refresh!
  [{{:keys [ssh-state]} :app :as payload}]
  (ssh/refresh! ssh-state)
  (handle-ssh-status! payload))

(defn handle-share-upload!
  [{{:keys [content]} :capture}]
  (let [url (kvs/create-share! {:content content})]
    (format "Share created at %s" url)))

(defn -alarm->hash
  [{:keys [id title]}]
  (str "@"
       (subs (md5sum [id title]) 0 4)))

(defn -print-alarm
  [{:keys [id title] {:keys [hours minutes]} :time :as alarm}]
  (format "(%02d:%02d) %s\n\tid = %s | hash %s"
          hours minutes title id (-alarm->hash alarm)))

(comment
  (-print-alarm {:id (random-uuid) :title "tty" :time {:hours 4 :mins 7}}))

(defn handle-list-alarms
  [{{registry :alarms} :app}]
  (let [alarms (alarm/list-alarms registry)
        alarm-details (map -print-alarm alarms)]
    (str/join "\n" alarm-details)))

(defn handle-set-alarm
  [{{:keys [scheduler] registry :alarms} :app
    {:keys [message time]} :capture}]
  (let [local-time (LocalTime/parse time)
        hours (.getHour local-time)
        minutes (.getMinute local-time)]
    (alarm/set-alarm!
     scheduler
     registry
     {:title message
      :time {:hours hours :minutes minutes}})
    "Alarm created"))

(defn handle-cancel-alarm
  [{{:keys [scheduler] registry :alarms} :app
    {:keys [id]} :capture}]
  (let [alarm-id (if (-hash? id)
                   (as-> (alarm/list-alarms registry) $
                     (-find-by-hash -alarm->hash $ id)
                     (:id $))
                   id)]
    (alarm/cancel-alarm! scheduler registry alarm-id)
    "Alarm cancelled"))

(declare handle-help-cmd)
(def commands
  [(cmd/->Command ["help"] #'handle-help-cmd)
   ; git commands
   (cmd/->Command ["git" "status" :repository] #'handle-git-status-cmd)
   (cmd/->Command ["git" "status"] (cmd/using-context #'handle-git-status-cmd
                                                      {:repository :cwd}))
   (cmd/->Command ["git" "attach" :repository "to" :branch] #'handle-git-attach-cmd)
   (cmd/->Command ["git" "attach" "to" :branch] (cmd/using-context #'handle-git-attach-cmd
                                                                   {:repository :cwd}))
   (cmd/->Command ["git" "detach" :repository] #'handle-git-detach-cmd)
   (cmd/->Command ["git" "detach"] (cmd/using-context #'handle-git-detach-cmd
                                                      {:repository :cwd}))
   (cmd/->Command ["git" "manage" :repository "with" :branch] #'handle-git-manage-cmd)
   (cmd/->Command ["git" "manage" "with" :branch] (cmd/using-context #'handle-git-manage-cmd
                                                                     {:repository :cwd}))
   (cmd/->Command ["git" "unmanage" :repository] #'handle-git-unmanage-cmd)
   (cmd/->Command ["git" "unmanage"] (cmd/using-context #'handle-git-unmanage-cmd
                                                        {:repository :cwd}))
   (cmd/->Command ["git" "pin" :repository] #'handle-git-pin-cmd)
   (cmd/->Command ["git" "pin"] (cmd/using-context #'handle-git-pin-cmd
                                                   {:repository :cwd}))
   (cmd/->Command ["git" "unpin" :repository] #'handle-git-unpin-cmd)
   (cmd/->Command ["git" "unpin"] (cmd/using-context #'handle-git-unpin-cmd
                                                     {:repository :cwd}))
   (cmd/->Command ["git" "enter" :branch] (cmd/using-context #'handle-git-enter-cmd
                                                             {:repository :cwd}))
   (cmd/->Command ["git" "exit"] (cmd/using-context #'handle-git-exit-cmd
                                                    {:repository :cwd}))
   (cmd/->Command ["git" "repair" :repository] #'handle-git-repair-cmd)
   (cmd/->Command ["git" "repair"] (cmd/using-context #'handle-git-repair-cmd
                                                      {:repository :cwd}))
   (cmd/->Command ["git" "sync"] #'handle-git-sync-cmd)
   (cmd/->Command ["git" "clean" "branches"] #'handle-git-clean-branches)
   (cmd/->Command ["git" "check" "branches"] #'handle-git-check-branches)
   ; meta commands
   (cmd/->Command ["status"] #'handle-status-cmd)
   ; plex commands
   (cmd/->Command ["plexify" "serie" :name :file-or-directory] #'handle-plex-series-cmd)
   ; news commands
   (cmd/->Command ["news"] #'handle-news-cmd)
   (cmd/->Command ["news" "all"] #'handle-news-all-cmd)
   (cmd/->Command ["news" "count"] #'handle-news-count-cmd)
   (cmd/->Command ["news" "purge"] #'handle-news-purge-cmd)
   ; keyvault commands
   (cmd/->Command ["keyvault" "list"] #'handle-keyvault-list-cmd)
   (cmd/->Command ["keyvault" "get" :key] #'handle-keyvault-get-cmd)
   (cmd/->Command ["keyvault" "set" :key :value] #'handle-keyvault-set-cmd!)
   (cmd/->Command ["keyvault" "unset" :key] #'handle-keyvault-unset-cmd!)
   ; ssh commands
   (cmd/->Command ["ssh"] #'handle-ssh-status!)
   (cmd/->Command ["ssh" "status"] #'handle-ssh-status!)
   (cmd/->Command ["ssh" "refresh"] #'handle-ssh-refresh!)
   ; share links
   (cmd/->Command ["share" "upload" :content] #'handle-share-upload!)
   ; alarms
   (cmd/->Command ["alarms"] #'handle-list-alarms)
   (cmd/->Command ["alarms" "set" :message "at" :time] #'handle-set-alarm)
   (cmd/->Command ["alarms" "cancel" :id] #'handle-cancel-alarm)])

(defn handle-help-cmd
  ; must be defined after the definition of the commands, to access them
  "Prints the help: list of all commands, etc."
  [_]
  (with-out-str
    (println "Application version: " app-version "\n")
    (doseq [cmd commands]
      (print " $ ")
      (println (str/join " " (:pattern cmd))))))

;;; -- Application stuff

(defn read-app-config
  "Reads the configuration of the application from the env"
  []
  {:data-dir (or (System/getenv "JARVIS_DATA_DIR") "/tmp/jarvis-data")
   :repl-port (Integer/parseInt (or (System/getenv "JARVIS_REPL_PORT") "5555"))})

(defn dispatch-input
  "Dispatch the user input command to the appropriate service."
  [app {:keys [cwd args]}]
  (try
    (->> args
         (cmd/match-command commands)
         (cmd/execute-matching-command {:app app :context {:cwd cwd}}))
    (catch RuntimeException e
      (.printStackTrace e)
      (str "[Error]" (.getMessage e)))))

(defn dispatch
  "Dispatch all commands to the services"
  [system inputs]
  (doall (map (partial #'dispatch-input system) inputs)))

(defn resolve-config-file
  "Builds the absolute path to a file from the application config."
  [config file-name]
  (-> (:data-dir config)
      (fs/path file-name)
      fs/absolutize
      fs/canonicalize
      str))

(defn start-scheduler!
  [system _app-config]
  (let [scheduler (s/create)]
    (s/start! scheduler)
    (swap! system assoc :scheduler scheduler)))

(defn start-ssh!
  [system app-config]
  (let [config (ssh/init app-config)]
    (ssh/start! config)
    (swap! system assoc :ssh-state config)))

(defn start-git-module!
  [system app-config]
  (let [git-module (git-core/init {:config-file (resolve-config-file app-config "git.edn")})
        git-scheduler (git-task/init app-config)]
    (git-task/start! git-scheduler
                     git-module
                     (:scheduler @system)
                     (:news @system)
                     (:ssh-state @system))
    (swap! system assoc
           :git git-module
           :git-scheduler git-scheduler)))

(defn start-plex-module!
  [system app-config]
  (let [plex-module (plex/init {:config-file (resolve-config-file app-config "plex.edn")})]
    (swap! system assoc :plex plex-module)))

(defn start-news-module!
  [system app-config]
  (let [news-module (notifs/init app-config)
        _ (notifs/start! news-module)]
    (swap! system assoc :news news-module)))

(defn start-keyvault-module!
  [system app-config]
  (let [keyvault-module (kv/init app-config)]
    (kv/start! keyvault-module)
    (swap! system assoc :keyvault keyvault-module)))

(defn get-news-count-msg
  "Returns the number of unread news if positive, wrapped in an array."
  [{:keys [news]}]
  (let [n (notifs/count-new-notifications news)]
    (when (pos? n)
      [(format "%d unread news" n)])))

(defn start-server-instance!
  [system _app-config]
  (let [server-instance (server/create! {:dispatch #(dispatch @system %)
                                         :before-disconnection #(get-news-count-msg @system)})]
    (server/start server-instance)
    (swap! system assoc :server server-instance)))

(defn bootstrap-application!
  [system]
  (let [system @system
        {scheduler :git-scheduler} system]
    (git-task/trigger-update! scheduler)
    (async/thread
      (check-new-version-available! system))))

(defn start-application!
  [app-config]
  (let [system (atom {})
        alarm-registry alarm/alarms]
    (reset! alarm-registry [])
    (doto system
      (start-scheduler! app-config)
      (start-ssh! app-config)
      (start-news-module! app-config)
      (start-git-module! app-config)
      (start-plex-module! app-config)
      (start-keyvault-module! app-config)
      (start-server-instance! app-config)
      (swap! assoc :alarms alarm-registry)
      (bootstrap-application!))))

(defn get-application-port
  [application]
  (server/get-server-port (:server @application)))

(defn stop-application!
  [app]
  (let [[app _] (swap-vals! app dissoc :server)]
    (if (:server app)
      (do
        (server/stop (:server app))
        (notifs/stop! (:news app))
        (kv/stop! (:keyvault app))
        (git-task/stop! (:git-scheduler app))
        :ok)
      :noop)))

(defonce *app nil)
(defn -reset
  [config]
  (when *app
    (stop-application! *app))
  (let [config (or config (read-app-config))
        new-app (start-application! config)]
    (alter-var-root (var *app) (constantly new-app))
    ; create one notification
    (notifs/notify! (@*app :news)
                    (java.util.UUID/randomUUID)
                    "Welcome my friend")
    (get-application-port *app)))

(comment
  (-reset nil)
  (-reset {:data-dir (str (fs/path (System/getProperty "user.home") ".config" "jarvis"))})
  (stop-application! *app)
  (get-application-port *app)

  (git-task/trigger-update! (:git-scheduler @*app))
  (git-task/trigger-update! (:git-scheduler @*app) "/mnt/grishka/Projets/CodinGame")

  (keys @*app)
  (:git @*app)

  (do
    (defn get-cwd!
      []
      (fs/real-path (fs/path ".")))

    (defn shell-dispatch
      "Calls `dispatch` from the current pwd"
      [input & {:keys [cwd]
                :or {cwd (get-cwd!)}}]
      (let [args (str/split input #"\s")]
        (dispatch @*app [{:cwd cwd :args args}]))))
  (shell-dispatch "help")
  (shell-dispatch "status")
  (shell-dispatch "news count")
  (shell-dispatch "git sync")
  (shell-dispatch "git status")
  (shell-dispatch "git manage /mnt/grishka/Projets/CodinGame with master")
  (shell-dispatch "git attach /mnt/grishka/Projets/iron-mansion/webapps/dev to main")
  (shell-dispatch "git detach /mnt/grishka/Projets/iron-mansion/webapps")
  (shell-dispatch "alarms")

  (s/schedule-next-operation! (:scheduler @*app))
  (s/status (:scheduler @*app) :print true)
  (notifs/count-new-notifications (:news @*app))
  (notifs/get-all-notifications (:news @*app))
  (git-task/trigger-project-update!
   (:git-scheduler @*app)
   "/mnt/grishka/Projets/CodinGame")

  (def state (:git @*app))
  (def server (:server @*app))
  (:plex @*app))
