(ns jarvis.cmd.command)

(defn match-input
  [pattern input]
  (when (= (count pattern) (count input))
    (loop [res {}
           entries (map vector pattern input)]
      (if-let [[p i] (first entries)]
        (cond
          (not (and p i)) nil
          (keyword? p) (recur (assoc res p i)
                              (next entries))
          (= p i) (recur res
                         (next entries)))
        res))))

(defn match [pattern input] (match-input pattern input))

(comment
  (def pattern [:key "=" :value])

  (def input ["k" "=" "v"])
  (def result {:key "k" :value "v"})

  (= (match-input pattern input) result)
  (= (match pattern input) result)

  ; not successful
  (nil? (match pattern ["k" "="]))
  (nil? (match pattern ["k" "=" "v" "and" "more"])))

(defn use-context-as-captures
  [capture context mapping]
  (->> mapping
       (map (fn [[src dst]] [src (get capture src) (get context dst)]))
       (filter (comp nil? second))
       (map (fn [[k _  v]] [k v]))
       (into capture)))

(comment
  (def capture {:a 1})
  (def context {:cwd "/path" :other 3})
  (def mapping {:b :cwd :a :other})
  (use-context-as-captures capture context mapping))

(defn using-context
  "Creates a new function trying to fill payload paremeters from the context."
  [f mapping]
  (fn [payload]
    (let [adapted (update payload
                          :capture
                          use-context-as-captures (:context payload) mapping)]
      (f adapted))))

(defrecord Command [pattern action])

(defn find-matching-commands
  "Finds all commands matching a given text input."
  [commands args]
  (vec
   (->> commands
        (map :pattern)
        (map #(match-input % args))
        (map vector commands)
        (filter (comp not nil? second)))))

(defn match-command
  "Finds the single command matching the input _or_ throws."
  [commands input]
  (let [matching-commands (find-matching-commands commands input)]
    (case (count matching-commands)
      0 (throw (IllegalArgumentException. (str "No match for input: " input)))
      1 (first matching-commands)
      (throw (IllegalArgumentException. (str "Too many matches for input: " matching-commands))))))

(defn resolve-command-action
  "Resolves the action as a function."
  [{:keys [action]}]
  (cond
    (symbol? action) (resolve action)
    (var? action) (deref action)
    :else action))

(defn execute-matching-command
  [payload [command matches]]
  ((resolve-command-action command)  (assoc payload :capture matches)))
