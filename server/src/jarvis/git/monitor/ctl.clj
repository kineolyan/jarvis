(ns jarvis.git.monitor.ctl
  (:require [jarvis.config :as config]
            [clojure.core.reducers :as r]
            [missionary.core :as m]
            [jarvis.git.exec :as git]
            [kineolyan.shellby :as shb]))

; status for an entry
; ::dirty = need to operate on the data
; ::pristine = synchronized, not operated
; ::updating = processed by the updating system
; ::pinned = do not touch, working in this dir
;
; ::updating -> ;;dirty = in process but new config required
; ;;pristine -> ::dirty = to process
; ::updating -> ::pristine = handled and no more to do

(defn create-state-entry
  [entry]
  (merge {:status ::dirty} entry)) ; TODO extract the info from the entry

(defn create-state
  [entries]
  (atom (map create-state-entry entries)))

(defn strip-entry
  [entry]
  (dissoc entry :status))

(defn strip-content
  [entries]
  (->> entries (r/map strip-entry) (into [])))

(defn save-config
  [k _r _before after]
  (config/write-config k (strip-content after)))

(defn init
  "Creates the initial state for this module, passing the options.
   - :config-file path to read and store the module configuration"
  [{:keys [config-file]}]
  (config/register-config ::git config-file)
  (let [config (or (config/read-config ::git) [])
        rconfig (create-state config)]
    (add-watch rconfig ::git save-config)
    {::config rconfig}))

(comment
  (def state (init {:config-file "/tmp/git.edn"})))

(defn create-matcher-for
  [k v]
  (fn [entry] (= (get entry k) v)))

(comment
  (def f (create-matcher-for :a "a"))
  (f {:a "a"})
  (f {:b "a" :a "b"}))

(defn create-git-matcher
  [repository]
  (try
    (let [git-dir (shb/m-sh? git/$resolve-root repository)]
      (create-matcher-for :git git-dir))
    (catch Exception _e
      (constantly false))))

(defn remove-entry
  "Removes an entry from the configuration."
  [config repository]
  (remove (create-git-matcher repository) config))

(defn add-entry
  "Adds the entry to the configuration."
  [config entry]
  (conj
   (remove-entry config (:git entry))
   (create-state-entry entry)))

(defn set-git-root!
  [{:keys [git] :as definition}]
  (assoc
   definition
   :git-root
   (shb/m-sh? git/$resolve-root git)))

(defn set-default-mode
  "Sets the mode as :tracked if not set in the repository definition."
  [definition]
  (update definition :mode #(or % :tracked)))

(defn complete-definition
  [definition]
  (-> definition
      (set-git-root!)
      (set-default-mode)))

(comment
  (complete-definition {:branch "master"
                        :git "/mnt/projects/web/oem"}))

(defn -record-entry!
  [state definition]
  (swap!
   (::config state)
   add-entry
   (complete-definition definition)))

(defn -delete-entry!
  [state repository]
  (swap! (::config state) remove-entry repository))

(defn attach!
  "Records that the repository to this module.
  It takes care of keeping it up-to-date for the branch."
  [state repository branch]
  (-record-entry! state {:git repository
                         :branch branch}))

(defn detach!
  "Detaches a repository from this module."
  [state repository]
  (-delete-entry! state repository))

(comment
  (def repo "/mnt/Grishka/Projets/iron-mansion/jarvis")
  (attach! state repo "main")
  (detach! state repo))

(defn get-entry
  "Gets the entry from configuration for the wanted repository."
  [config repository]
  (->> config
       (filter (create-git-matcher repository))
       first))

(defn update-entry!
  "Updates the specified entry and returns its new value.
   This also modifies the current state. The returned value is simply a shortcut to read the edited entry from the state."
  [state repository f]
  (let [new-config (swap! (::config state)
                          (fn [config]
                            (if-let [entry (get-entry config repository)]
                              (conj (remove-entry config repository) (f entry))
                              (do (println "No entry to update for " repository)
                                  config))))]
    (get-entry new-config repository)))

(defn pin!
  "Pins a git repository to the current branch. This will not be updated during
  during the current session.
  _state_ represents the current git config and _repository_ is the path
  to the target repository."
  [state repository]
  (update-entry! state repository #(assoc % :status ::pinned)))

(defn unpin!
  "Unpins a git repository. Synchronization will resume for this.
  _state_ represents the current git config and _repository_ is the path to the
  target repository."
  [state repository]
  (update-entry! state repository #(if (= (:status %) ::pinned)
                                     (assoc % :status ::dirty)
                                     %)))

(comment
  (pin! state repo)
  (unpin! state repo))

(defn reset-to-branch
  [config repository]
  (if-let [{:keys [git branch]} (get-entry config repository)]
    (shb/m-sh? git/$checkout git (str "origin/" branch))
    (str "No record for " repository)))

(defn enter!
  [state repository branch]
  (pin! state repository)
  (shb/m-sh? git/$checkout repository branch))

(defn exit!
  [state repository]
  (reset-to-branch @(::config state) repository)
  (unpin! state repository))

(defn mark-as-broken!
  [state repository]
  (update-entry! state repository #(assoc % :status ::broken)))

(defn repair!
  "Repairs a broken git repository.
  Synchronization may mark a repository as broken after errors. This resets this
  marker.
  _state_ represents the current git config and _repository_ is the path to the
  target repository."
  [state repository]
  (update-entry! state repository #(if (= (:status %) ::broken)
                                     (assoc % :status ::dirty)
                                     %)))

(defn calculate-compute-state
  "Compute the state transition before running an execution loop."
  [{:keys [status] :as state}]
  (case status
    ::pinned state ; do not change anything
    ::broken state ; cannot process
    ::updating state ; already updating the repository
    ::pristine (assoc state :status ::updating)
    ::dirty (assoc state :status ::updating)))

(defn mark-for-compute!
  "Marks the repository for computation before running an execution loop.
  Returns the state of the repository after the marking."
  [state repository]
  (update-entry! state repository calculate-compute-state))

(defn calculate-updated-state
  [{:keys [status] :as state}]
  (case status
    ::pinned state ; do not change anything
    ::broken state ; cannot process
    ::updating (assoc state :status ::pristine) ; already updating the repository
    ::pristine state ; already clear by another process
    ::dirty (assoc state :status ::updating)))

(defn mark-as-updated!
  "Marks the repository as updated.
   This returns the new state of the repository, possibly still marks for compute if a concurrent operation marked it as dirty."
  [state repository]
  (update-entry! state repository calculate-updated-state))

(defn -build-cleaning-message
  "Builds the report message after cleaning a repository from deleted branches."
  [deleted-branches uncleanable-branches]
  (if (or (seq deleted-branches) (seq uncleanable-branches))
    (concat
     (when (seq deleted-branches) ["Deleted branches"])
     (map (fn [{:keys [branch sha]}] (str " - " branch " (" sha ")")) deleted-branches)
     (when (seq uncleanable-branches) ["/!\\ To delete but checked out:"])
     (map #(str " * " (:branch %)) uncleanable-branches))
    ["Nothing to do"]))

(defn has-deletable-branches?!
  "Tests if the given path contains branches that can be deleted"
  [path]
  (m/?
   (m/sp
    (let [all-branches (shb/m-sh? git/$get-branch-statuses! path)
          branches (filter git/delete-branch-candidate? all-branches)
          branch-names (map :branch branches)]
      (seq branch-names)))))

(defn clean-deleted-branches!
  "Cleans the deleted branches not checked from a repository at `path`."
  [path & {:keys [dry-run]
           :or {dry-run false}}]
  (m/?
   (m/sp
    (let [all-branches (shb/m-sh? git/$get-branch-statuses! path)
          branches (filter git/delete-branch-candidate? all-branches)
          branch-names (map :branch branches)]
      (when-not dry-run
        (shb/m-sh? git/$delete-branches path branch-names))
      (let [uncleanable-branches (filter git/uncleanable-branch-candidate? all-branches)]
        (concat
         (when dry-run ["-- (Dry-run mode) --"])
         (-build-cleaning-message branches uncleanable-branches)))))))

(comment
  (mark-as-broken! state repo)
  (repair! state repo)
  (mark-for-compute! state repo)
  (mark-as-updated! state repo)

  (clean-deleted-branches! "/mnt/grishka/Projets/iron-mansion/jarvis" :dry-run true))

(defn manage!
  "Adds the repository to this module for automatic management.
  Managed repositories are automatically synced with their remote branches and new changes are
  automatically committed and pushed to the remote."
  [state repository branch]
  (-record-entry! state {:git repository
                         :branch branch
                         :mode :managed}))

(defn unmanage!
  "Disable automatic management for a repository."
  [state repository]
  (-delete-entry! state repository))
