(ns jarvis.git.monitor.task
  (:require
   [clojure.set :as set]
   [missionary.core :as m]
   [taoensso.timbre :as timbre]
   [kineolyan.shellby :as shb :refer [m-sh?]]
   [kineolyan.ssh :as ssh]
   [kineolyan.netfuse :as netfuse]
   [jarvis.git.exec :as exec]
   [jarvis.git.monitor.ctl :as ctl]
   [jarvis.notifications :as notif]
   [jarvis.scheduler :as s]))

;;; -- Utility functions --

;;; -- Project process

(defn update-entry?
  [{:keys [status]}]
  (= status ::ctl/updating))

(defn -get-git-ssh-details
  [config git-host]
  (->> [git-host :wildcard]
       (keep (fn [hostname]
               (first (filter #(= hostname (:hostname %)) config))))
       first))

(comment
  (def ssh-conf (ssh/read-config))
  (-get-git-ssh-details ssh-conf "gitlab")
  (-get-git-ssh-details ssh-conf "github.com"))

(defn -?get-repository-status
  [ssh-config git-dir]
  (m/sp
   (let [git-url (m-sh? exec/$get-remote git-dir)
         ssh-details (-get-git-ssh-details ssh-config (:host git-url))]
     {:hostname (:hostname ssh-details)
      :unlocked? (not (m-sh? ssh/$require-passphrase? ssh-details))})))

(defn -?do-checkout
  [git remote-name branch]
  (m/sp
   (try
     (m-sh? exec/$checkout git (str remote-name "/" branch))
     (timbre/infof "Repository %s updated!" git)
     (catch Exception e
       ; check that it may be an expected case of failed check
       (cond
         ; The repository is dirty
         (seq (m-sh? exec/$status git))
         (timbre/infof "Repository %s not updated because of uncommitted changes" git)

         ; Transient error in the repository
         (when-let [result (ex-data e)]
           (re-matches #".*is at .+ but expected .+" (:err result)))
         (do (timbre/infof "Stale repository %s. Attempting some cleaning until next time" git)
             (m-sh? exec/$prune-remote git remote-name))

         ; Not a known case, just rethrow
         :else
         (throw e))))))

; NOTE fetch can happen concurrently but it should not be an issue
(defn ?perform-update
  [{:keys [git branch notification-service ssh-db]}]
  (m/sp
   (timbre/debug "Updating" git)
   (let [result (m/? (-?get-repository-status @ssh-db git))
         unlocked? (:unlocked? result)
         git-hostname (:hostname result)
         notification-key [::git-host git-hostname]]
     (if unlocked?
       (do
         (netfuse/run-with-fuse!
          #(m-sh? exec/$fetch git))
         (let [current-branch (m-sh? exec/$get-current-branch git)]
           (timbre/debugf "Branch for %s: `%s` - target=%s" git current-branch branch)
           (when (= current-branch "HEAD")
             (m/? (-?do-checkout git "origin" branch)))
           (timbre/info "Update complete for" git))
         (when notification-service
           (notif/discard!
            notification-service
            notification-key))
         :updated)
       (do
         (timbre/info git " locked by ssh passphrase. Skipping...")
         (when notification-service
           (notif/notify!
            notification-service
            notification-key
            (format "Git host '%s' is locked by a passphrase" git-hostname)))
         :locked)))))

; TODO detect if the branch has changed
; TODO find the real remote of the branch
; Get the name of the current branch: git rev-parse --abbrev-ref HEAD
;   - detached => "HEAD"
;   -> master => "master"
; Get the name of the branch + remote : git rev-parse --abbrev-ref --symbolic-full-name @{u}
;  -> error when detached

(defn ?perform-sync
  [{:keys [git branch notification-service ssh-db]}]
  (m/sp
   (timbre/debug "Syncing" git)
   ;; Save any changes to the repository
   (when (seq (m-sh? exec/$status git))
     (m-sh? exec/$add git)
     (let [commit-payload {:message "Auto-commit"
                           :author "Jarvis <>"}
           commit-result (m-sh? exec/$commit git commit-payload)
           committed-branch (:branch commit-result)
           sha1 (:sha1 commit-result)]
       (when (and committed-branch sha1)
         (timbre/debugf
          "Changes saved in commit <%s> on branch [%s]"
          sha1
          committed-branch))))
   (let [result (m/? (-?get-repository-status @ssh-db git))
         unlocked? (:unlocked? result)
         git-hostname (:hostname result)
         notification-key [::git-host git-hostname]]
     (if unlocked?
       (do
         ;; Fetch all remote information
         (netfuse/run-with-fuse!
          #(m-sh? exec/$fetch git))
         ;; Merge with the local branch (if needed and if fetched)
         (let [current-branch (m-sh? exec/$get-current-branch git)]
           (timbre/debugf "Branch for %s: `%s` - target=%s" git current-branch branch)
           (when (= current-branch branch)
             (let [result (m-sh? exec/$merge git (str "origin/" branch))]
               (when (and notification-service (= result :ko))
                 (notif/notify!
                  notification-service
                  [::project git :sync]
                  (format "Cannot merge %s with its remote" git))))
             (netfuse/run-with-fuse!
              #(m-sh? exec/$push git "origin" [branch])))
           (timbre/info "Update complete for" git))
         ;; Push (if needed)
         :synced)
       (do
         (timbre/info git " locked by ssh passphrase. Skipping...")
         (when notification-service
           (notif/notify!
            notification-service
            notification-key
            (format "Git host '%s' is locked by a passphrase" git-hostname)))
         :locked)))))

(comment
  (def coding-git "/mnt/grishka/Projets/CodinGame")
  (m-sh? exec/$fetch coding-git)
  (m-sh? exec/$status coding-git)
  (m-sh? exec/$add coding-git)
  (m-sh? exec/$commit coding-git {:message "hello"
                                  :author "Testing <>"})

  (m/?
   (?perform-sync {:branch "master"
                   :git "/mnt/grishka/Projets/CodinGame"
                   :ssh-db (atom (ssh/read-config))})))

(defn do-update!
  "Performs the update in a blocking operation.
  Returns `:success` or `:error` depending on the outcome of the update operation."
  [{:keys [git mode notification-service] :as payload}]
  (try
    (let [action (if (= mode :managed)
                   (?perform-sync payload)
                   (?perform-update payload))
          result (m/? action)]
      [:success result])
    (catch Exception e
      (.printStackTrace e)
      (when notification-service
        (notif/notify!
         notification-service
         [::project git :error]
         (format "Sync error for %s: %s" git (.getMessage e))))
      [:error e])))

(comment
  (do
    (def notif-srv (notif/init nil))
    (notif/start! notif-srv))
  (m/?
   (-?get-repository-status ssh-conf "/mnt/grishka/Projets/iron-mansion/reading-buffer"))
  (m/?
   (-?get-repository-status ssh-conf "/mnt/grishka/Projets/CodinGame"))
  (m/?
   (?perform-update {:branch "master"
                     :git "/mnt/grishka/Projets/CodinGame"
                     :notification-service notif-srv
                     :ssh-db (atom (ssh/read-config))}))
  (m/?
   (?perform-update {:branch "main"
                     :git "/mnt/grishka/Projets/iron-mansion/reading-buffer"
                     :ssh-db (atom (ssh/read-config))
                     :notification-service notif-srv}))
  (m/?
   (shb/exec shb/missionary-executor (exec/$fetch "/mnt/grishka/Projets/CodinGame")))
  (m-sh? exec/$checkout "/mnt/grishka/Projets/CodinGame" "master")
  (m-sh? exec/$status  "/mnt/grishka/Projets/CodinGame")
  (do-update! {:branch "master"
               :git "/mnt/grishka/Projets/CodinGame"
               :mode :managed
               :ssh-db (atom (ssh/read-config))}))

; TODO convert this whole block to missionary, natively managed by the scheduler
; Beware of interruption in the middle of the process
(defn trigger-project-update!
  [self project-key]
  (timbre/trace "About to run update for %s" project-key)
  (let [git-db (:git-db @self)
        repository (get-in @self [:projects project-key :repository])
        state (ctl/mark-for-compute! git-db repository)
        _ (timbre/tracef "Project %s marked before compute" project-key)]
    (if (update-entry? state)
      (let [_ (timbre/tracef "Starting update of %s" project-key)
            [result _] (do-update! (merge @self state))
            _ (timbre/tracef "Update of %s completed" project-key)]
        (if (= result :success)
          (ctl/mark-as-updated! git-db repository)
          (ctl/mark-as-broken! git-db repository)))
      (timbre/infof "Skipping update of %s" project-key))))

;;; -- Project control

(def
  ^{:doc "Default to 5 min"}
  default-time-between-updates {:min 5})

(defn create-project-state
  [project-key]
  (let [schedule-id [::project project-key]]
    {:repository project-key
     :updated-at nil
     :schedule-id schedule-id}))

(defn start-project!
  "Schedules updates for a new project, returning the project state ."
  [self project-key]
  (timbre/debug "starting" project-key)
  (let [project-state (create-project-state project-key)]
    (s/schedule! (:scheduler @self)
                 {:schedule-id (:schedule-id project-state)
                  :interval default-time-between-updates
                  :action #(trigger-project-update! self project-key)})
    (swap!
     self
     #(update % :projects assoc project-key project-state))))

(defn update-project!
  "Triggers a project update, returning a Deref indicating the end of the update."
  [self project-key]
  (timbre/debug "update project" project-key)
  (s/execute-now! (:scheduler @self)
                  (get-in @self [:projects project-key :schedule-id])))

(defn stop-project!
  "Stops a project."
  [self project-key]
  (s/deschedule! (:scheduler @self)
                 (get-in @self [:projects project-key :schedule-id]))
  (swap!
   self
   #(update % :projects dissoc project-key)))

;;; -- Manager process

(defn list-git-projects
  [git-db]
  (map :git git-db))

(defn list-projects-to-add
  [git-db projects]
  (let [registered-repos (keys projects)
        existing-repos (list-git-projects git-db)]
    (set/difference (set existing-repos)
                    (set registered-repos))))

(defn list-projects-to-update
  [git-db projects]
  (let [registered-repos (keys projects)
        existing-repos (list-git-projects git-db)]
    (set/intersection (set registered-repos)
                      (set existing-repos))))

(defn list-projects-to-delete
  [git-db projects]
  (let [registered-repos (keys projects)
        existing-repos (list-git-projects git-db)]
    (set/difference (set registered-repos)
                    (set existing-repos))))

(defn classify-projects
  [git-db projects]
  {:added (list-projects-to-add git-db projects)
   :updated (list-projects-to-update git-db projects)
   :deleted (list-projects-to-delete git-db projects)})

(defn update-projects!
  [self ks]
  (timbre/debug "updating projects")
  (doseq [k ks]
    (update-project! self k)))

(defn start-projects!
  [self ks]
  (timbre/debug "starting new projects")
  ; TODO this is not the cleaner way of starting the projects
  ; it would be better to do it unitary
  (doseq [k ks]
    (start-project! self k)))

(defn stop-deleted-projects!
  [self ks]
  (timbre/debug "deleting projects")
  (doseq [k ks]
    (stop-project! self k)))

(defn update-all-projects!
  [self]
  (timbre/debug "updating all projects")
  (let [state @self
        git-db (-> state :git-db ::ctl/config deref)
        {:keys [added updated deleted]} (classify-projects git-db (:projects state))]
    (doto self
      (stop-deleted-projects! deleted)
      (start-projects! added)
      (update-projects! updated))))

(defn stop-all-projects!
  [self]
  (doseq [k (-> @self :projects keys)]
    (stop-project! self k)))

(defn -collect-repository-roots
  [{:keys [git-db]}]
  (->> git-db
       ::ctl/config
       deref
       (map :git-root)
       (into #{})))

(defn -collect-repository-states
  [paths]
  (->> paths
       (map #(vector % (ctl/has-deletable-branches?! %)))
       (into {})))

(defn -report-deletable-branches!
  [self]
  (timbre/debug "Starting branch cleaning detection")
  (let [{:keys [notification-service] :as state} @self
        repositories (-collect-repository-roots state)
        repository-states (-collect-repository-states repositories)]
    (doseq [[path to-clean?] repository-states
            :when to-clean?]
      (notif/notify!
       notification-service
       [::branch-gc path]
       (format "Project %s has branches to clean" path))))
  (timbre/debug "Branch cleaning detection completed"))

(comment
  (require '[jarvis.server])
  (def s
    (:git-scheduler @jarvis.server/*app))
  (-collect-repository-roots @s)
  (-collect-repository-states *1)
  (-report-deletable-branches! s))

;;; -- Manager control

(defn init
  [_options]
  (atom {}))

(defn -start-branch-gc!
  [self]
  (s/schedule!
   (:scheduler @self)
   {:schedule-id ::branch-gc
    :interval {:min 10}
    :action #(-report-deletable-branches! self)})
  nil)

(defn start!
  [self git-state scheduler notification-service ssh-state]
  (reset! self {:projects {}
                :ssh-db ssh-state
                :git-db git-state
                :scheduler scheduler
                :notification-service notification-service})
  (-start-branch-gc! self)
  nil)

(defn stop!
  [self]
  (stop-all-projects! self)
  (reset! self {}))

(defn trigger-update!
  "Triggers an update of projects.
  If `project` is passed, it updates only one project. Otherwise, all projects are updated."
  ([self]
   (update-all-projects! self))
  ([self project]
   (update-projects! self [project])))

(comment
  (def self (:git-scheduler @jarvis.server/*app))
  (swap! self assoc :projects {}) ; restart the state
  (trigger-update! self)
  (:git-db @self))
