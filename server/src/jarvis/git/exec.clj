(ns jarvis.git.exec
  (:require [clojure.string :as str]
            [babashka.fs :as fs]
            [missionary.core :as m]
            [kineolyan.shellby :as shb]
            [kineolyan.ssh :as ssh]))

;;; Git actions

(defn -parse-git-url
  [url]
  (when-let [[_ user host] (re-find #"([\w\-]+@)?(.+?):.+" url)]
    {:user (when user (subs user 0 (dec (.length user))))
     :host host}))

(defn $get-remote
  [dir]
  {:args '(git remote get-url origin)
   :dir dir
   :f #(-> % shb/check :out str/trim-newline -parse-git-url)})

(defn $fetch
  [dir]
  {:args '(git fetch --all --prune --tags)
   :dir dir
   :env (ssh/create-env-for-ssh)
   :f shb/check})

(comment
  (def clovis-dir "/mnt/grishka/Projets/iron-mansion/clovis")
  (shb/sh $fetch clovis-dir)
  (shb/sh $get-remote clovis-dir)
  (-parse-git-url "git@github.com:user/repo.git")
  (-parse-git-url "ssh-alias:user/repo.git"))

(defn $checkout
  [dir branch & {:keys [track]
                 :or {track false}}]
  {:args (concat ["git" "checkout" branch] (when track ["--track"]))
   :env (ssh/create-env-for-ssh)
   :dir dir
   :f shb/check})

(defn $get-current-branch
  "Gets the name of the given branch"
  [dir & {:keys [commit] :or {commit "HEAD"}}]
  {:args ["git" "rev-parse" "--abbrev-ref" commit]
   :dir dir
   :f #(->> % shb/check :out str/trim)})

(defn $get-current-sha1
  "Gets the sha256 of the given branch"
  [{:keys [dir branch] :or {branch "HEAD"}}]
  {:args ["git" "rev-parse" "--short" branch]
   :dir dir
   :f #(->> % shb/check :out str/trim)})

(defn $resolve-root
  "Gets the root of the git project, containing .git."
  [repository]
  {:args '(git rev-parse --show-toplevel)
   :dir repository
   :f #(->> % shb/check :out str/trim)})

(comment
  (shb/m-sh? $resolve-root "/mnt/grishka/Projets/iron-mansion/webapps/dev"))

(def branch-gone-pattern #"(?<checked>[*+])?\s+(?<branch>[\w\-_./]+)\s+(?<sha>[a-f0-9]{6,})\s+(?<worktree>\(.+?\)\s+)?(?:\[[\w/\-_./]+(?<gone>: gone)?])?")
(def detached-pattern "(HEAD detached at")

(comment
  ; easily test the pattern to see when it fails
  (let [s "+ feature/5.12/improve-cross-package-detection            3c9dc00b609 (/workspaces/5.11/std) [origin/feature/5.12/improve-cross-package-detection: gone] Correct script."]
    (re-find branch-gone-pattern s)))

(defn parse-branch-status
  "Parses the plain status of a git line.
  If the status does not represent any branch, it returns `nil`."
  [row]
  (when-not (str/includes? row detached-pattern)
    (let [matcher (re-matcher branch-gone-pattern row)]
      (if (.find matcher)
        {:branch (.group matcher "branch")
         :sha (.group matcher "sha")
         :checked (some? (.group matcher "checked"))
         :gone (some? (.group matcher "gone"))}
        (throw (ex-info "Cannot parse branch status" {:input row}))))))

(defn $get-branch-statuses!
  "Gets the statuses of the branch in the current directory"
  [dir]
  {:args ["git" "branch" "-vv"]
   :dir dir
   :f #(->> %
            shb/check
            :out
            (str/split-lines)
            (keep parse-branch-status)
            (filter some?))})

(defn delete-branch-candidate?
  "Decides a branch can be deleted based on its branch status."
  [{:keys [gone checked]}]
  (and gone (not checked)))

(defn uncleanable-branch-candidate?
  "Decides a branch has been deleted but cannot be removed because it is checked out somewhere."
  [{:keys [gone checked]}]
  (and gone checked))

(defn $delete-branches
  "Deletes the git branches from the repository at `dir`."
  [dir branches]
  ; we have to do `--delete --force`, aka `-D` because often, branches are merged after
  ; merge with _main_ and with squashing, generating various false positives about not-merged
  {:args (concat ["git" "branch" "--delete" "--force"] branches)
   :dir dir
   :f shb/check})

(comment
  ($get-branch-statuses! ".")
  (shb/sh $get-branch-statuses! ".")
  (filter delete-branch-candidate? (shb/sh $get-branch-statuses! "."))

  ($delete-branches "some/path" ["a" "b-c/to-e"]))

(defn $clone
  [{:keys [url branch target-dir]}]
  {:args (concat ["git" "clone" url "--branch" branch "--depth" "2"]
                 (some-> target-dir str vector))
   :env (ssh/create-env-for-ssh)
   :f shb/check})

;;; Scary Internet actions

; TODO how migrate this to shellby?
(defn get-repository-head!
  "Returns the sha1 of the commit for a given repository."
  [{:keys [url branch] :or {branch "main"}}]
  (m/?
   (m/sp
    (let [repo-uuid (java.util.UUID/randomUUID)
          repo-path (str (fs/path "/tmp" (str "jarvis-" repo-uuid)))]
      (try
        (shb/m-sh? $clone {:url url :branch branch :target-dir repo-path})
        (shb/m-sh? $get-current-sha1 {:dir repo-path :branch branch})
        (finally (fs/delete-tree repo-path)))))))

#_(defn $get-repository-head!
    "Returns the sha1 of the commit for a given repository."
    [{:keys [url branch] :or {branch "main"}}]
    (let [repo-uuid (java.util.UUID/randomUUID)
          repo-path (str (fs/path "/tmp" (str "jarvis-" repo-uuid)))]
      {:deps [[$clone {:url url :branch branch :target-dir repo-path}]
              [$get-current-sha1 {:dir repo-path :branch branch}]]
       :sync true
       :f nil}
    ; TODO find a way to pass a finally action
    ; (finally (fs/delete-tree repo-path))
      (ex-info "TODO" {})))

(comment
  (get-repository-head! {:url "https://gitlab.com/kineolyan/jarvis"}))

(defn -parse-change-line
  [line]
  (let [[_status filename] (str/split line #"\s+")]
    filename))

(defn $status
  "Returns the list of changed files from a given dir using `git status`."
  [dir]
  ;; Could use --porcelain=2, for a machine friendly output
  {:args ["git" "status" "--porcelain"]
   :dir dir
   :f #(->> %
            shb/check
            :out
            (str/split-lines)
            (map str/trim)
            (remove str/blank?)
            (map -parse-change-line))})

(comment
  (shb/exec shb/sh-executor ($status "."))
  (shb/exec shb/sh-executor ($status "../abc")))

(defn $prune-remote
  [dir remote]
  {:args ["git" "remote" "prune" remote]
   :dir dir
   :env (ssh/create-env-for-ssh)
   :f shb/check})

(defn $add
  [dir & paths]
  {:args (concat ["git" "add"]
                 (if (seq paths) paths ["."]))
   :dir dir
   :f shb/check})

(comment
  ; Add everything (".")
  ($add "some/dir")
  ; Add a selection of dir
  ($add "some/dir" "path/to/a/file" "the/main/dir"))

;; Sample output: we want master and the sha1
;; [master 7b63d6f] test
;; 1 file changed, 0 insertions(+), 0 deletions(-)
;; create mode 100644 coucou
(defn -parse-commit-output
  [output]
  (let [[line] (str/split-lines output)
        pattern #"^\[([^\s]+) ([a-fA-F0-9]+)\].*"]
    (when-let [[_ branch sha1] (re-find pattern line)]
      {:branch branch
       :sha1 sha1})))

(defn $commit
  ([dir] ($commit dir {}))
  ([dir {:keys [message author]}]
   {:args (concat ["git" "commit"]
                  (when-not (str/blank? message) ["-m" message])
                  (when-not (str/blank? author) [(str "--author=" author)]))
    :dir dir
    :env {"LANG" "C"}
    :f #(-> % shb/check :out -parse-commit-output)}))

(comment
  (-parse-commit-output
   "[master 7b63d6f] test
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 coucou")
  (-parse-commit-output "oops")
  ($commit "some/dir")
  ($commit "some/dir" {:message "Auto-commit"
                       :author "Me <myself@here>"}))

(defn $merge
  "Merges the current branch with a remote branch.
  This returns `:ok` or `:ko` depending on the success of the operation."
  [dir branch]
  {:args ["git" "merge" "--no-ff" branch]
   :dir dir
   :f #(if (zero? (:exit %)) :ok :ko)})

(defn $push
  [dir remote branches]
  {:pre [(seq branches)]}
  {:args (concat ["git" "push" remote] branches)
   :dir dir
   :env (ssh/create-env-for-ssh)
   :f shb/check})

(comment
  ($push "here" "upstream" ["br/anch" "other"])
  ;; will fail because no branch are provided
  ($push "here" "origin" nil))
