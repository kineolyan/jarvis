(ns jarvis.alarm
  (:require [taoensso.timbre :as timbre]
            [kineolyan.shellby :as shb]
            [jarvis.scheduler :as scheduler])
  (:import [java.time LocalTime]))

(defonce alarms (atom []))

(def -notify-levels
  "List of existing levels in `notify-levels"
  #{:low :normal :critical})

(defn $notify-send
  "Creates the command for a notification send using `notify-send`."
  [{:keys [title message level duration]
    :or {message ""
         title "Jarvis notification"
         duration 5}}]
  (let [valid-level (or (some #{level} -notify-levels) :normal)
        level-option (name valid-level)]
    {:args ["notify-send" title message
            "--urgency" level-option
            "--expire-time" (* duration 1000)]
     :f shb/check}))

(comment
  (shb/m-sh?
   (constantly {:args ["notify"] :f identity}))
  (shb/m-sh? $notify-send {:message "hello" :level :critical}))

(defn -same-day?
  [dt1 dt2]
  (= (.toLocalDate dt1) (.toLocalDate dt2)))

(defn time->epoch-sec
  "Converts a datetime to a daily epoch in second"
  [java-time]
  (+ (* 60 (.getHour java-time))
     (.getMinute java-time)))

(defn create-alarm-notif-args
  [target-datetime current-datetime {:keys [title]}]
  (let [current-epoch (time->epoch-sec current-datetime)
        target-epoch (time->epoch-sec target-datetime)
        diff (- target-epoch current-epoch)]
    (cond
      (>= 30 diff 28)
      {:title title
       :message "Start thinking about it"
       :level :low
       :duration 5}
      (>= 20 diff 18)
      {:title title
       :message "Time to consider it"
       :level :normal
       :duration 10}
      (>= 10 diff 8)
      {:title title
       :message "Now is a good time to do it"
       :level :normal
       :duration 30}
      (>= 5 diff 0)
      {:title title
       :message "Wake up and go for it"
       :level :critical
       :duration 30}
      :else nil)))

(defn operate-alarm
  [target-time current-time alarm]
  (timbre/tracef "Waking %s" (:id alarm))
  (when-let [notify-args (create-alarm-notif-args
                          target-time
                          current-time
                          alarm)]
    (timbre/debugf "Alarm notification for %s with %s"
                   (:id alarm)
                   (pr-str (:args notify-args)))
    (shb/m-sh? $notify-send notify-args)))

(comment
  (let [target (LocalTime/now)
        other (.plusMinutes target -27)]
    [target other (create-alarm-notif-args target other {:title "Il faut partir"})]))

(defn -create-alarm-task
  [{:as alarm
    :keys [id]
    {:keys [hours minutes]} :time}]
  (let [ref-time (LocalTime/of hours minutes)]
    {:schedule-id [::id id]
     :interval {:min 1}
     :action #(operate-alarm ref-time (LocalTime/now) alarm)}))

(defn set-alarm!
  "Creates an alarm in the scheduler and records it to the registry atom."
  [scheduler registry alarm]
  (let [alarm (assoc alarm :id (random-uuid))]
    (swap! registry conj alarm)
    (scheduler/schedule!
     scheduler
     (-create-alarm-task alarm)))
  nil)

(defn cancel-alarm!
  "Stops an alarm from the scheduler and removes it from the registry atom"
  [scheduler registry id]
  (let [alarm-id (if (uuid? id) id (parse-uuid id))]
    (swap! registry remove #(= alarm-id (:id %)))
    (scheduler/deschedule! scheduler [::id alarm-id]))
  nil)

(defn list-alarms
  [registry]
  (map #(select-keys % [:title :time :id]) @registry))

(comment
  (def alarm {:id "bureau" :title "go"
              :time {:hours 18 :minutes 0}})
  (let [d1 (LocalTime/now)
        d2 (.plusMinutes d1 -17)]
    (create-alarm-notif-args d1 d2 alarm))
  (let [{:keys [action]}
        (-create-alarm-task alarm)]
    (action))

  ; testing with a local scheduler
  (def scheduler (scheduler/create))
  (scheduler/start! scheduler)
  (set-alarm! scheduler alarms alarm)
  (list-alarms alarms)
  (scheduler/execute-now!
   scheduler
   [::id (-> @alarms first :id)])

  (cancel-alarm! scheduler alarms "7f5f0e35-18e5-4372-9341-80dfcc670c61"))
