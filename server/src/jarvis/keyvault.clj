(ns jarvis.keyvault
  (:require [clojure.string :as str]))

;;; keyvault setup

(defn init
  [_options]
  (atom nil))

(defn start!
  [service]
  (reset! service {}))

(defn stop!
  [service]
  (reset! service nil))

;;; keyvault operators

(defn add-entry!
  "Adds the entry `k` to the service, with value `v`."
  [service k v]
  {:pre [(some? k)
         (not (str/blank? k))
         (some? v)]}
  (swap! service assoc k v))

(defn find-entry
  "Finds the entry `k` in the service.
  Returns nil if the entry is absent."
  [service k]
  (get @service k))

(defn remove-entry!
  "Removes the entry `k` from the service.
  This is a no-op if the entry is absent from the system."
  [service k]
  (swap! service dissoc k))

(defn list-entries
  "List all keys of the vault. Secret values are not displayed."
  [service]
  (keys (deref service)))

(comment
  (def service (init nil))
  (start! service)
  (stop! service))
