(ns jarvis.plex.core
  (:require [babashka.fs :as fs]
            [jarvis.config :as config]))

(def generated-fake-root "<enter your real Plex root directory here>")
(defn generate-config
  []
  (config/write-config ::plex {::root generated-fake-root}))

(defn valid-config?
  [{root-dir ::root}]
  (and (some? root-dir)
       (string? root-dir)
       (fs/directory? root-dir)))

(defn read-config
  "Reads the configuration for this module.
   A valid configuration must be defined for this to work."
  []
  {:post [(some? %) (valid-config? %)]}
  (config/read-config ::plex))

(defn ensure-valid-config
  [config]
  (when-not (valid-config? config)
    (throw (ex-info "Cannot run plex without a valid config"
                    {:config-file (config/get-config-file ::plex)}))))

(defn init
  "Creates the initial state for this module.
   This does not return any state. It must be fetched every time an action must access it."
  [{:keys [config-file]}]
  (config/register-config ::plex config-file)
  (when-not (config/read-config ::plex)
    (generate-config)))

(defn warn-and-ignore-files
  [file]
  (if-not (fs/exists? file)
    (println (str "File " file " does not exist"))
    true))

(defn collect-files
  [files-or-directories]
  (into []
        (comp
         (map fs/absolutize)
         (filter warn-and-ignore-files))
        (flatten [files-or-directories])))

(comment
  (collect-files "/mnt/grishka/Media/Vuze/His.Dark.Materials.S03.COMPLETE.720p.iP.WEBRip.x264-GalaxyTV[TGx]"))

(def episode-patterns
  [#"[sS](\d{1,2})[eE](\d{1,2})"
   #"^(?:.+[.\-_ /])?(\d{1,2})x(\d{1,2})[.\-_ /]"])

(defn parse-info
  [file pattern]
  (when-let [[_ s e] (re-find pattern file)]
    {::season (Integer/parseInt s)
     ::episode (Integer/parseInt e)}))

(defn guess-episode-info
  [file]
  (reduce
   #(when-let [metadata (parse-info (str file) %2)]
      (reduced metadata))
   nil
   episode-patterns))

(defn make-target-path
  "Makes the path for the episode if "
  [config serie-name season episode extension]
  (let [tv-show-root (::root config)
        season-dir (format "Season %02d" season)
        file-name (format "%s - s%02de%02d.%s" serie-name season episode extension)]
    (fs/file tv-show-root serie-name season-dir file-name)))

(defn add-to-tv-library
  [source-file target-file]
  (if-not (fs/exists? target-file)
    (do
      ; Ensure the directory exist before linking
      (fs/create-dirs (fs/parent target-file))
      (fs/create-link target-file source-file)
      (str "Added <" source-file "> to <" target-file ">"))
    (str "Already present: " target-file)))

(defn add-episode-if-matching
  [config serie-name episode-file]
  (if-let [{::keys [season  episode]} (guess-episode-info episode-file)]
    (let [target-file (make-target-path  config  serie-name season episode (fs/extension episode-file))]
      (add-to-tv-library episode-file target-file))
    (str "Cannot infer info about " episode-file)))

(defn add-serie
  "Transform every file or directory provided into entries for the serie.
   This guesses the number of the serie episode from the file name.
   It creates hard links for each episode whose name was guessed."
  [_state {::keys [serie-name files-or-directories]}]
  (let [config (read-config)
        episodes (collect-files files-or-directories)]
    (doall
     (map #(add-episode-if-matching config serie-name %) episodes))))

(comment
  (init {:config-file (fs/path (fs/home) ".config" "jarvis" "plex.edn")})
  (def vuze (fs/path "/mnt/grishka/Media/Vuze"))
  (def payload {::serie-name "His Dark Materials"
                ::files-or-directories (map #(fs/path vuze (fs/path %))
                                            ["His.Dark.Materials.S02E01.720p.HDTV.x265-MiNX[TGx]/His.Dark.Materials.S02E01.720p.HDTV.x265-MiNX.mkv"
                                             "His.Dark.Materials.S02E03.HDTV.x264-PHOENiX[TGx]/His.Dark.Materials.S02E03.HDTV.x264-PHOENiX.mkv"])})
  (collect-files (::files-or-directories payload))
  (add-serie nil payload)
  (add-serie nil {::serie-name "Some-serie"
                  ::files-or-directories ["/not/a/path"]}))
