(ns jarvis.scheduler
  (:require [clojure.string :as str]
            [clojure.pprint :as pp]
            [missionary.core :as m]
            [taoensso.timbre :as timbre])
  (:import
   [java.time Instant]))

(defn -n-args [f]
  (-> f class .getDeclaredMethods first .getParameterTypes alength))

(defn -create-action
  "Creates an action compatible with missionary that can be run."
  [f]
  (case (-n-args f)
    ; standard function without args, consider a blocking call
    0 (m/via-call m/blk f)
    ; assume that this is a standard missionary-like task
    2 f))

;;; Scheduler functions

(def units
  (as-> {:ms 1} m
    (assoc m :sec (* 1000 (get m :ms)))
    (assoc m :min (* 60 (get m :sec)))
    (assoc m :hour (* 60 (get m :min)))))

(defn interval->millis
  ([interval]
   (->> interval
        (map #(apply interval->millis %))
        (reduce +)))
  ([unit n]
   {:pre [(contains? units unit)]}
   (* n (get units unit))))

(defn get-operation-schedules
  "Computes the next execution times for all operations not being executed.
  Schedules are returned as `{:operation <k> :at <time>}` "
  [state]
  (into []
        (comp
         (filter #(= :idle (-> % second :status)))
         (map (fn [[k {:keys [last-execution interval]}]]
                {:operation k :at (+ last-execution (interval->millis interval))})))
        (:operations state)))

(defn -transition-status
  [state k f]
  (update-in state [:operations k :status] f))

(defn mark-execution
  [state k]
  (-transition-status
   state
   k
   #(case %
      :idle :executing
      :executing :pending
      :pending :pending)))

(defn mark-executed
  [state k]
  (-transition-status
   state
   k
   #(case %
      :executing :idle
      :pending :executing
      :idle :idle)))

(defn stamp-execution
  "Records the timestamp of the action execution end."
  [state k]
  (update-in state [:operations k]
             assoc :last-execution (System/currentTimeMillis)))

(defn record-canceller
  [state k f]
  (update-in state [:operations k] assoc :canceller f))

(defn remove-canceller
  [state k]
  (update-in state [:operations k] dissoc :canceller))

(defn take-schedule-ticket!
  [scheduler]
  (swap! scheduler update-in [:scheduling :ticket] inc))

(defn valid-ticket?
  [state ticket]
  (= (get-in state [:scheduling :ticket]) ticket))

(defn get-first-schedule
  [schedules now]
  (->> schedules
       (filter #(> (:at %) now))
       (sort-by :at)
       (first)))

(defn get-past-operations
  [schedules now]
  (filter #(<= (:at %) now) schedules))

(defn create-scheduling-action
  [schedules now f]
  (when (seq schedules)
    (let [past-operations (get-past-operations schedules now)
          first-schedule (get-first-schedule schedules now)
          waiting-time (when first-schedule (- (:at first-schedule) now))
          trigger (m/dfv)
          action (m/sp
                  (when (m/? trigger)
                    (timbre/trace "Running due operations")
                    (doseq [entry past-operations]
                      (f (:operation entry)))
                    (when first-schedule
                      (timbre/trace "Waiting before next operation")
                      (m/? (m/sleep waiting-time))
                      (timbre/trace "Executing next operation")
                      (f (:operation first-schedule)))))]
      {:trigger #(trigger :go)
       :action action
       :waiting-time waiting-time})))

(comment
  (def schedules [])
  (def now 1500)
  (def a (create-scheduling-action
          schedules
          now
          #(tap> [:executing %])))
  (apply (:trigger a) [])
  (m/? (:action a)))

(defn create-scheduling
  "Creates the scheduling from the whole state.
  `f` is called with the operation key when an action is scheduled for execution. `f` can be called
  many times.
  It returns an entity with:
   - :action containing the missionary task running due tasks and waiting before running the first next task;
   - :trigger containing a 0-arity function to call to start the scheduling action;
   - :waiting-time with the time to wait in ms"
  [state f]
  (let [schedules (get-operation-schedules state)
        now (System/currentTimeMillis)]
    (create-scheduling-action schedules now f)))

(defn -cancelled-task?
  [err]
  (let [exception-class-name (-> err class Class/.getName)]
    ; quite brittle, but cannot access the true error type
    (str/ends-with? exception-class-name ".Cancelled")))

(defn apply-scheduling!
  [scheduler scheduling & {:keys [ticket] :or {ticket "N/A"}}]
  (let [on-success (fn [_res] (timbre/debugf "Scheduling completed {:ticket %d}" ticket))
        on-failure (fn [err] (if (-cancelled-task? err)
                               (timbre/debugf "Scheduling cancelled {:ticket %d}" ticket)
                               (timbre/warnf "Scheduling error {:ticket %d} -> %e"
                                             ticket
                                             (Throwable->map err))))
        new-cancel ((scheduling :action) on-success on-failure)
        update-state #(if (valid-ticket? % ticket)
                        (update % :scheduling assoc :next-action-cancel new-cancel)
                        %)
        [before-state after-state] (swap-vals! scheduler update-state)]
    (when (valid-ticket? after-state  ticket)
      {:to-cancel (get-in before-state [:scheduling :next-action-cancel])
       :trigger (:trigger scheduling)})))

(declare -execute!)
(defn schedule-next-operation!
  [scheduler]
  (timbre/trace "Planning delay before the next executions.")
  (let [state-with-ticket (take-schedule-ticket! scheduler)
        ticket (get-in state-with-ticket [:scheduling :ticket])]
    (when-let [scheduling (create-scheduling state-with-ticket #(-execute! scheduler %))]
      (timbre/tracef "Planning next wake in %d ms {:ticket %d}"
                     (:waiting-time scheduling)
                     ticket)
      (if-let [updated (apply-scheduling! scheduler scheduling :ticket ticket)]
        (do
          (timbre/tracef "Trigger the timeout action: waiting %d ms {:ticket %d}"
                         (:waiting-time scheduling)
                         ticket)
        ; call cancel for the previous state
          (apply (:to-cancel updated) [])
          (apply (:trigger updated) []))
        (timbre/tracef "Cancelling action {:ticket %d}" ticket)))))

(defn complete-execution!
  "Completes the execution of operation `k`."
  [scheduler k]
  (timbre/tracef "Execution of %s completed" k)
  (let [_ (swap! scheduler remove-canceller k)
        _ (swap! scheduler stamp-execution k)
        ; next call will make the operation "visible" after completion
        new-state (swap! scheduler mark-executed k)]
    (when (= :executing
             (get-in new-state [:operations k :status]))
      (-execute! scheduler k))
    (schedule-next-operation! scheduler)))

(defn -execute!
  [scheduler k]
  (timbre/tracef "Requesting execution of %s" k)
  (let [new-state (swap! scheduler mark-execution k)
        task-status (get-in new-state [:operations k :status])]
    (if (= :executing task-status)
      (let [_ (timbre/tracef "Planning execution of %s" k)
            f (get-in new-state [:operations k :action])
            after-exec (fn [_] (complete-execution! scheduler k))
            _ (timbre/tracef "Executing %s" k)
            cancel (f after-exec after-exec)]
        (swap! scheduler record-canceller k cancel))
      (timbre/infof "Execution of %s already in progress" k))))

(defn -remove-operation!
  "Removes operation `k` from the scheduler, returning the removed operation definition."
  [scheduler k]
  (let [[before _] (swap-vals! scheduler update-in [:operations] dissoc k)]
    (get-in before [:operations k])))

;;; Scheduler operations

(defn -valid-project?
  [scheduler k]
  (contains? (:operations @scheduler) k))

(defn execute-now!
  "Triggers the asynchronous execution of the operation id-ed with `k`.
  The execution is triggered after a possible run of the operation terminates. If the operation is interruptible, it is cancelled.
  If the operation is waiting to be executed while this is called, it is a no-op. Calls are not stacked."
  [scheduler k]
  {:pre [(-valid-project? scheduler k)]}
  (timbre/debugf "Requesting manually execution of %s" k)
  (-execute! scheduler k))

(defn schedule!
  "Registers a new operation.
  The operation starts asynchronously as this operation completes."
  [scheduler {:keys [schedule-id interval action]}]
  {:pre [(not (-valid-project? scheduler schedule-id))]}
  (let [entry {:interval interval
               :action (-create-action action)
               :status :idle
               :canceller nil
               :last-execution 0}]
    (swap! scheduler
           update-in [:operations]
           assoc schedule-id entry)
    (execute-now! scheduler schedule-id)))

(defn reschedule!
  "Updates the configuration of an existing operation.
  If the operation is currently running and the action changes, the new action is scheduled after the operation terminates.
  If the interval is updated and the new timeout is reached, the action is triggered asynchronously."
  [scheduler {:keys [schedule-id] :as definition}]
  {:pre [(-valid-project? scheduler schedule-id)]}
  (let [operation (-remove-operation! scheduler schedule-id)]
    ;; FIXME not compliant with the proposed design
    ;; We always schedule the operation after this call instead of waiting
    (when-let [cancel (:canceller operation)]
      (cancel))
    (schedule! scheduler (merge operation definition))))

(defn deschedule!
  "Removes a schedule, returning a IDeref completing when the action terminates.
  If its action is interruptible, it is cancelled."
  [scheduler k]
  {:pre [(-valid-project? scheduler k)]}
  (-remove-operation! scheduler k)
  (schedule-next-operation! scheduler)
  ;; FIXME we don't wait for all operations to complete
  (doto (promise)
    (deliver :done)))

(defn -get-operation-status
  [k operation]
  (-> operation
      (assoc :key k)
      (update :last-execution #(str (Instant/ofEpochMilli %)))
      (select-keys [:key :interval :status :last-execution])))

(defn status
  "Returns a table-friendly status of all operations."
  [scheduler & {:keys [] :as opts}]
  (let [report (map (fn [[k v]] (-get-operation-status k v))
                    (:operations @scheduler))]
    (when (:print opts)
      (pp/print-table report))
    report))

;;; Scheduler service

(defn create
  "Creates the status of the scheduler."
  []
  (atom {:status :stopped}))

(defn started?
  [scheduler]
  (= :started (:status scheduler)))

(defn start!
  "Starts the scheduler, immediately starting the registered actions."
  [scheduler]
  {:pre [(not (started? @scheduler))]}
  (swap! scheduler assoc
         :status :started
         :operations {}
         :scheduling {:ticket 0
                      :next-action-cancel (constantly nil)}))

(defn stop!
  "Stops the scheduler. It returns a IDeref completing when all registered actions have completed
  or been cancelled."
  [scheduler]
  ;; FIXME we must wait for all running actions
  (let [[before _] (swap-vals! scheduler assoc :status :stopped)]
    (if (started? before)
      (do ;; this is the first stop
        (reset! scheduler {:status :stopped})
        (doto (promise)
          (deliver :stopped)))
      (doto (promise)
        (deliver :noop)))))

(comment
  (def scheduler (create))
  (start! scheduler)
  (started? @scheduler)
  (stop! scheduler)

  ; schedule execution of (println "hello") every 3 min
  ; the operation is indexed by `:greet`
  (schedule! scheduler {:schedule-id :greet
                        :interval {:sec 10}
                        :action #(println "greetings to all")})
  ;; Manually executes the action
  (m/? (get-in @scheduler [:operations :greet :action]))
  ((get-in @scheduler [:operations :greet :action])
   (fn [_] (println :done))
   (fn [_] (println :error)))
  (swap! scheduler mark-executed :greet)

  ; execute now the action :greet and reset the last execution time
  ; after this call, :greet is reschedule for its interval
  ; Returns a Deref showh when the execution is complete
  (execute-now! scheduler :greet)

  ; failed because :greet is already scheduled
  (schedule! scheduler {:schedule-id :greet
                        :interval {:sec 2}
                        :action #(println "salutations")})
  ; should actually use reschedule! if needed
  ; pass options to update only the relevant parts
  (reschedule! scheduler {:schedule-id :greet
                          :interval {:sec 1}})
  (reschedule! scheduler {:schedule-id :greet
                          :action #(println "I <3 U")})

  ; remove a task
  ; schould not fail if the item in not present
  ; returns a Deref showing when that any run is complete and that the schedule is removed
  (deschedule! scheduler :greet)

  ; Working with a missionary action
  (schedule! scheduler {:schedule-id :mission
                        :interval {:sec 5}
                        :action (m/sp
                                 (tap> "greetings to all")
                                 (m/? (m/sleep 2000))
                                 (tap> "nice to meet yoll"))})
  (execute-now! scheduler :mission)
  (deschedule! scheduler :mission)

  ;; Constraints
  ;; :interruptible true if an action can be interrupted
  ;; must use the format described by missionary tasks, that is, return a function for cancellation
  ;; If not interruptible, schedule an execution after completion

  ;; Discover args of the function
  (->> #'deschedule!
       meta :arglists))
