(ns jarvis.netlify.kvs
  (:require [clojure.string :as str]
            [babashka.http-client :as http]
            [kineolyan.netfuse :as netfuse]))

(def netlify-root-url "https://skineo.netlify.app")

(defn url
  [& args]
  (str/join "/" args))

(defn -post-share!
  [passwd content]
  (let [result (http/post (url netlify-root-url "share")
                          {:headers {"X-Skineo-Secret" passwd}
                           :body content
                           :throws false})
        {:keys [status body]} result]
    (if (<= 200 status 299)

      (:body result)
      (throw (ex-info "Cannot create share" {:status status
                                             :body body})))))

(defn -get-pass-from-env
  []
  (System/getenv "NETLIFY_KVS_KEY"))

(comment
  (def r (-post-share!
          (-get-pass-from-env)
          "coucou")))

(defn create-share!
  "Gets the SHA1 of the latest version of the application, or `nil` if it cannot be retrieved."
  [{:keys [passwd content]}]
  (netfuse/run-with-fuse!
   -post-share!
   (or passwd (-get-pass-from-env))
   content))
