(ns jarvis.core-server
  (:gen-class)
  (:require [clojure.string :as str]
            [clojure.tools.cli :as cli]
            [jarvis.server :as app]
            [nrepl.server :as nrepl]))

(defn register-shutdown-hook
  "Create a new shutdown hook executing f."
  [f]
  (let [r (reify Runnable (run [_this] (f)))
        t (Thread. r "exit-hook")]
    (.addShutdownHook (Runtime/getRuntime) t)))

(defn start-repl-server
  [{:keys [repl-port]}]
  (println "Embedded REPL starting on port " repl-port)
  (nrepl/start-server :port repl-port))

(defn stop-repl-server
  [server]
  (nrepl/stop-server server))

(comment
  (def rp (start-repl-server {:repl-port 6667}))
  (stop-repl-server rp))

(def cli-options
  [["-d" "--daemon" "Runs as a daemon"]
   ["-h" "--help"]])

(defn usage [options-summary]
  (str/join
   \newline
   ["Usage: jarvis [options]"
    (str "Version: " app/app-version)
    ""
    "Options:"
    options-summary]))

(defn error-msg [errors]
  (str "The following errors occurred while parsing your command:\n\n"
       (str/join \newline errors)))

(defn validate-args
  "Validate command line arguments. Either return a map indicating the program
  should exit (with a error message, and optional ok status), or a map
  indicating the action the program should take and the options provided."
  [args]
  (let [{:keys [options arguments errors summary]} (cli/parse-opts args cli-options)]
    (cond
      (:help options)
      {:exit-message (usage summary) :ok? true}
      errors
      {:exit-message (error-msg errors)}
      :else
      {:arguments arguments :options options})))

(defn exit [status msg]
  (println msg)
  (System/exit status))

(defn run-daemon-mode
  [_application]
  (println "Server started. Press Ctrl+C to stop")
  (Thread/sleep (bit-shift-left 1 62)))

(defn run-interactive-mode
  [application]
  (println "Server started. Press Ctrl+C to stop or type (q)uit | e(x)it")
  (loop [cmd (read-line)]
    (case cmd
      ("quit" "q" "exit" "x")
      (do
        (app/stop-application! application)
        (System/exit 0))
      (recur (read-line)))))

(defonce running-application (atom nil))
(defn -main
  "Starts the application."
  [& args]
  (let [{:keys [options exit-message ok?]} (validate-args args)]
    (if exit-message
      (exit (if ok? 0 1) exit-message)
      (let [config (app/read-app-config)
            application (app/start-application! config)
            repl-server (start-repl-server config)]
        (reset! running-application application)
        (register-shutdown-hook #(do (println "exit from shutdown hook")
                                     (app/stop-application! application)
                                     (stop-repl-server repl-server)
                                     (reset! running-application nil)))
        (if (:daemon options)
          (run-daemon-mode application)
          (run-interactive-mode application))))))
