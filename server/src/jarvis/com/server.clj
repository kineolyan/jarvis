(ns jarvis.com.server
  (:require [net.tcp.server :as s]
            [clojure.java.io :as io]
            [clojure.string :as str]
            [clojure.core.match :refer [match]]
            [bencode.core :as b]
            [taoensso.timbre :as timbre])
  (:import [java.io FileNotFoundException PushbackInputStream]))

(def port-file "/tmp/jarvis.sock")

(defn read-port
  "Reads the port of the started Jarvis application if any or return nil."
  []
  (try
    (Integer/parseInt (slurp port-file))
    (catch FileNotFoundException _e nil)))

(defn read-bencode-input
  [reader]
  (let [read-value (b/read-bencode reader)]
    (read-string (String. read-value "UTF-8"))))

(defn read-input
  "Reads the input from the reader as a list of lines"
  [reader]
  (let [ben-reader (PushbackInputStream. reader)]
    [(read-bencode-input ben-reader)]))

(defn send-answer
  [writer payload]
  (b/write-bencode writer (pr-str payload))
  (.flush writer))

(defn send-output
  [writer output]
  (->> (flatten [output])
       (str/join "\n")
       (hash-map :output)
       (send-answer writer)))

(defn exec-hook
  [writer hook]
  (when-let [output (seq (hook))]
    (send-output writer output)))

(defn handler [{:keys [reader writer dispatch after-connection before-disconnection]
                :or {after-connection (constantly nil)
                     before-disconnection (constantly nil)}}]
  (timbre/trace "new connection :D")
  (try
    (exec-hook writer after-connection)
    (let [[input] (read-input reader)
          [output] (dispatch [input])]
      (match [output]
        [[:raw value]]
        (send-output writer value)
        :else
        (do (send-output writer output)
            (exec-hook writer before-disconnection))))
    (send-answer writer {:exit :success})
    (catch Exception e
      (.printStackTrace e))
    (finally (timbre/trace "bye connection ;)"))))

(defn create!
  "Creates a new server.
  The config requires a dispatch function at :dispatch. It accepts optional functions
  :after-connection and :before-disconnection."
  [config]
  (s/tcp-server
   :port 0
   :handler (s/wrap-streams #(do
                               (timbre/trace "new stream opened")
                               (handler (assoc config
                                               :reader %1
                                               :writer %2))))))

(defn get-server-port
  [server]
  (let [^java.net.ServerSocket socket @(:socket server)]
    (.getLocalPort socket)))

(defn start
  "Starts the given server."
  [server]
  (s/start server)
  (let [port (get-server-port server)]
    (if-not (.exists (io/file port-file))
      (spit port-file port)
      (println (str "Server already started on port " (read-port) ". For this server, set port to " port)))))

(defn stop
  "Stops a running server"
  [server]
  (when (s/running? server)
    (s/stop server)
    (when-let [file-port (read-port)]
      (when (= file-port (get-server-port server))
        (io/delete-file port-file)))))

(comment
  (def server (create! {:dispatch (partial map #(str "echo <" % ">"))}))
  (start server)
  (s/running? server)
  (read-port)

  (stop server)

  (.getLocalPort @(:socket server)))
