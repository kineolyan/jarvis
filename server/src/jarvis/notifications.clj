(ns jarvis.notifications)

; The service stores two values:
; - the last time a message was read
; - the list of messages passed to the service
;
; Messages are identified by their ids.
; If an existing message with the same id exists, it is removed and the new one is inserted
; An alternative implementation would be to keep all messages for history tracking.

(defn -remove-message
  [messages id]
  (remove #(= id (:id %)) messages))

(defn -append-message
  [state id message]
  (update
   state
   :messages
   (fn [messages]
     (-> messages
         (-remove-message id)
         (conj {:id id
                :time (System/currentTimeMillis)
                :content message})))))

(defn get-new-messages
  [ref-time messages]
  (filter #(> (% :time) ref-time) messages))

(defn -update-read-time
  [state]
  (assoc state :read-time (System/currentTimeMillis)))

;;; -- service control --

(defn init
  [_options]
  (atom nil))

(defn start!
  [service]
  (reset! service {:read-time 0
                   :messages []}))

(defn stop!
  [service]
  (reset! service nil))

(defn notify!
  "Creates a new notification in the system."
  [service id message]
  (swap! service -append-message id message))

(defn discard!
  "Removes a notification from the system, given its id."
  [service id]
  (swap! service update :messages -remove-message id))

(defn purge!
  "Removes all notifications from the history"
  [service]
  (swap! service update :messages (constantly [])))

(defn count-new-notifications
  [service]
  (let [{:keys [read-time messages]} @service]
    (count (get-new-messages read-time messages))))

(defn find-new-notifications!
  "Retrieves the new notifications since the last read. This updates the read timestamp in the system."
  [service]
  (let [[original-state _] (swap-vals! service -update-read-time)]
    (get-new-messages (original-state :read-time) (original-state :messages))))

(defn get-all-notifications
  [service]
  (get-new-messages 0 (@service :messages)))

(comment
  (def srv (init nil))
  (start! srv)

  (notify! srv 1 "coucou")
  (count-new-notifications srv)
  (find-new-notifications! srv)
  (get-all-notifications srv)

  ;; update the notification
  (notify! srv 2 "error somewhere")
  (notify! srv 1 "greetings"))
