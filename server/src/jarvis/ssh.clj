(ns jarvis.ssh
  (:require [kineolyan.ssh :as ssh]
            [kineolyan.shellby :as shb]
            [babashka.fs :as fs]))

;;; Actions

(defn refresh!
  [self]
  (reset! self (ssh/read-config)))

(defn init
  [_]
  (atom nil :meta {:name :ssh-config}))

(defn start!
  [self]
  (refresh! self))

(defn stop!
  [self]
  (reset! self nil))

(defn -collapse-home
  "Collapse the home path at the start of path."
  [path]
  (let [home (fs/home)]
    (if (fs/starts-with? path home)
      (str "~" (subs (str path) (count (str home))))
      (str path))))

(defn generate-report
  [self]
  (map
   (fn [{:keys [hostname] identity-file :identity :as entry}]
     {:hostname hostname
      :file (when identity-file (-collapse-home identity-file))
      :protected (when identity-file true)
      :locked (when (shb/m-sh? ssh/$require-passphrase? entry) true)})
   @self))

(comment
  (do
    (def conf (init nil))
    (start! conf))
  (shb/m-sh? ssh/$read-loaded-keys)
  (shb/m-sh? ssh/$read-signature (-> conf deref first :identity))
  (shb/m-sh? ssh/$require-passphrase? (first @conf))

  (generate-report conf))
