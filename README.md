# Jarvis

Computer assistant for various random tasks.

## Features

### Git

Maintain registered repositories synchronized with their main branch.

### Keyvault storage

Stores secrets values in the daemon memory, without any particular encryption.<br/>
This is convenient to share secret keys, passwords across various shell sessions.

Commands:

 - `jarvis keyvault set <key> <value>` to add a new value
 - `jarvis keyvault get <key>` to read a value

### SSH status

Inspect your SSH configuration at _~/.ssh/config_. This displays which hosts are defined, the identify file protected them and if the identity file is loaded by an ssh-agent.

Command: `jarvis ssh`

## Installation

Any JRE 11+ is required to run the daemon. Install whichever you want and add it to your path.<br>
[Babashka](https://github.com/babashka/babashka#quickstart) is used to run the client talking to the daemon.

 1. Create the directory `~/bin`, where the server and client will be installed.
 1. Run the automatic script `scripts/install.sh`.<br/>
    One-liners:
     - Bash, zsh, ...<br>
       `bash < <(curl -sL https://gitlab.com/kineolyan/jarvis/-/raw/main/scripts/install.sh)`
     - Fish<br>
       `curl -sL https://gitlab.com/kineolyan/jarvis/-/raw/main/scripts/install.sh | bash`
    This installs a client `jarvis` and the daemon `jarvis-server.jar` in `~/bin`.
 1. Run `java -jar ~/jarvis-server.jar` to start the daemon. It creates a file _/tmp/jarvis.sock_ storing the port listened by the daemon for actions.
 1. Run `~/bin/jarvis help` or `bb ~/bin/jarvis help` to display the various available commands.

### Bugs

Surely some, but not bothering me on a daily basis :)

## Developing on Jarvis

**Jarvis** is developed with the latest version of Clojure, using Clojure deps to manage dependencies.<br/>
Babashka tasks are available to assist the developer experience.

## License

Copyright © 2020

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

This Source Code may also be made available under the following Secondary
Licenses when the conditions for such availability set forth in the Eclipse
Public License, v. 2.0 are satisfied: GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or (at your
option) any later version, with the GNU Classpath Exception which is available
at https://www.gnu.org/software/classpath/license.html.
