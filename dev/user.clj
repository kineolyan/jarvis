(ns user
  (:require [playback.preload]
            [clojure.string :as str]
            [babashka.fs :as fs]
            [clojure.test :as t]))

(comment
  ; update dependencies
  (clojure.repl.deps/sync-deps)
  )

;;; utility functions to call in the REPL

(defn -list-project-files
  []
  (fs/glob "." "**/*.clj"))

(defn -test-file?
  [f]
  (str/ends-with? (fs/file-name f) "_test.clj"))

(defn -project-ns?
  [nms]
  (str/starts-with? (ns-name nms) "jarvis."))

(defn -test-ns?
  [nms]
  (str/ends-with? (ns-name nms) "-test"))

(defn -list-project-namespaces
  []
  (->> (all-ns)
       (sort-by ns-name)
       (filter -project-ns?)))

(defn load-test-files
  []
  (let [test-files (filter -test-file? (-list-project-files))]
    (doseq [f test-files]
      (load-file (str f)))
    (println "-- Test files loaded --")))

(defn test-project
  []
  (let [test-nss (filter -test-ns? (-list-project-namespaces))]
    (apply t/run-tests test-nss)))

; Load all test namespaces at start of the REPL, as they are the one we want to test
; Newly added namespaces will be manually
(load-test-files)

(comment
  (-list-project-namespaces)
  (test-project))

;;; Portal utilities
;; -----------------
;; Only load them if portal is activated

#_(when (find-ns 'portal.api)
  ;; Manually require the namespace
    (defn setup-portal []
      (require '[portal.api])

      (def ptl (#'portal.api/open)) ; Open a new inspector
      (add-tap #'portal.api/submit) ; Add portal as a tap> target
      (comment
        (deref ptl))))
