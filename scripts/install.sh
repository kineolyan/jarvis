#!/bin/bash
set -e

INSTALL_DIR=$HOME/bin
SERVER_FILE=jarvis-server.jar
CLIENT_FILE=jarvis
[ -n "$1" ] && MAIN_BRANCH="$1" || MAIN_BRANCH="main"

SERVER_PATH="$INSTALL_DIR/$SERVER_FILE"
CLIENT_PATH="$INSTALL_DIR/$CLIENT_FILE"

mkdir -p "$INSTALL_DIR"
echo "Installing client and server to $INSTALL_DIR"

curl -sL https://gitlab.com/kineolyan/jarvis/-/jobs/artifacts/$MAIN_BRANCH/raw/binaries/jarvis-server.jar\?job\=deploy-server -o "$SERVER_PATH"
curl -sL https://gitlab.com/kineolyan/jarvis/-/raw/$MAIN_BRANCH/client.bb -o "$CLIENT_PATH"

# Sanity checks
if [ -f "$SERVER_PATH" ]
then
	echo -e "\e[34mServer installed as $SERVER_PATH\e[0m"
else
	echo -e "\e[31mFailed to install server (expected a file $SERVER_PATH)\e[0m"
fi

if [ -f "$CLIENT_PATH" ]
then
	chmod +x "$CLIENT_PATH"
	echo -e "\e[34mClient installed as $CLIENT_PATH\e[0m"
else
	echo -e "\e[31mFailed to install client (expected a file $CLIENT_PATH)\e[0m"
fi

