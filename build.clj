(ns build
  (:require [clojure.tools.build.api :as b]))

(def version "2.0")
(def git-sha (b/git-process {:git-args "rev-parse --short HEAD"}))
(def git-rev (b/git-count-revs nil))
(def class-dir "target/server/classes")
(def basis (b/create-basis {:project "deps.edn"}))
(def uber-file (format "target/jarvis-server-app-b%s.jar" git-rev))

(defn report
  [msg]
  (printf "-- %s --%n" msg))

(defn clean [_]
  (b/delete {:path "target/server"})
  (report "Project clean"))

(defn build [_]
  (b/compile-clj {:basis basis
                  :src-dirs ["server/src"]
                  :class-dir class-dir
                  :java-opts [(format "-Djarvis.sha1=%s" git-sha)
                              (format "-Djarvis.version=%sb%s-%s" version git-rev git-sha)]})
  (report "Compilation completed"))

(defn uber [_]
  (build nil)
  (b/uber {:class-dir class-dir
           :uber-file uber-file
           :main 'jarvis.core-server
           :basis basis})
  (report "Uberjar generated"))
