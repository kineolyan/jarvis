#!/usr/bin/env -S rlwrap bb
(ns client
  (:gen-class)
  (:require [clojure.string :as str]
            [clojure.stacktrace :as trace]
            [clojure.tools.cli :as cli]
            [bencode.core :as b])
  (:import [java.net Socket]
           [java.io FileNotFoundException]))

;;; Client helpers

(def port-file "/tmp/jarvis.sock")
(def log-file "/tmp/jarvis-client-log.txt")

(defn read-port
  "Reads the port of the started Jarvis application."
  []
  (try
    (Integer/parseInt (slurp port-file))
    (catch FileNotFoundException _ nil)))

(defn read-secret!
  [& {:keys [label]}]
  (let [console (System/console)
        template (if label
                   "Secret (%s) = "
                   "Secret value = ")]
    (String. (.readPassword console template (to-array [label])))))

(defn value->secret
  "Transforms a user-value `@s:<name>` into a secret value read from stdin."
  [^String user-value]
  (println "--" user-value "--")
  (let [label (when (> (.length user-value) 3)
                (subs user-value 3))]
    (read-secret! :label label)))

(defn value->env
  "Transforms a user-value `@e:<name>` into a secret value read from stdin."
  [^String user-value]
  (if-let [label (when (> (.length user-value) 3)
                   (subs user-value 3))]
    (System/getenv label)
    (throw (ex-info "Incorrect pattern for param from env" {:value user-value}))))

(defn interpret-value!
  "Processes user values to transform them if needed."
  [user-value]
  (condp #(str/starts-with? %2 %1) user-value
    "@s:" (value->secret user-value)
    "@e:" (value->env user-value)
    user-value))

(defn process-payload
  [payload]
  (cond
    (nil? payload)
    {:exit :none}
    (contains? payload :output)
    (println (payload :output))
    (contains? payload :exit)
    {:exit :success}
    :else
    {:exit :error
     :reason "Invalid message"
     :message payload}))

(defn write-msg
  [writer msg]
  (let [encoded (pr-str msg)]
    (b/write-bencode writer encoded)))

(defn read-msg
  [reader]
  (let [received (b/read-bencode reader)]
    (read-string (String. received "UTF-8"))))

(defn send-msg!
  [port content]
  (let [s (Socket. "localhost" port)
        out (.getOutputStream s)
        _ (write-msg out content)
        in (java.io.PushbackInputStream. (.getInputStream s))]
    (loop []
      (let [answer (read-msg in)
            res (process-payload answer)]
        (if (contains? res :exit)
          (when-not (= (res :exit) :success)
            (binding [*out* *err*]
              (println res)))
          (recur))))))

;;; Program

(defn cwd
  []
  (System/getProperty "user.dir"))

(defn build-payload
  [args cwd]
  {:cwd cwd :args (if (seq args) args ["status"])})

(defn create-message!
  [args]
  (build-payload (map interpret-value! args) (cwd)))

(def cli-options
  [["-p" "--port PORT" "Port number"
    :parse-fn #(Integer/parseInt %)
    :validate [#(< 0 % 0x10000) "Must be a number between 0 and 65536"]]
   ["-n" "--dry-run" "Runs in debug mode"
    :default false]
   ["-h" "--help" "Prints this message"]])

(defn usage [options-summary]
  (str/join
   \newline
   ["Usage: jarvis [options] arguments"
    ""
    "Options:"
    options-summary
    ""
    "Actions:"
    "  help: prints all supported actions"
    ""
    "Special values:"
    (str
     "This client supports passing special values to the server."
     " These values follow the format `@<type>:<key>`."
     " They act as placeholders in the command, being replaced by the interpreted values.")
    "Supported values:"
    " * @s:<label> reads a secret value, asking for it the provided label"
    " * @e:<label> reads the env var named from the label"]))

(defn error-msg [errors]
  (str "The following errors occurred while parsing your command:\n\n"
       (str/join \newline errors)))

(defn validate-args
  "Validate command line arguments. Either return a map indicating the program
  should exit (with a error message, and optional ok status), or a map
  indicating the action the program should take and the options provided."
  [args]
  (let [{:keys [options arguments errors summary]} (cli/parse-opts args cli-options)]
    (cond
      (:help options)
      {:exit-message (usage summary) :ok? true}
      errors
      {:exit-message (error-msg errors)}
      :else
      {:arguments arguments :options options})))

(defn exit [status msg]
  (println msg)
  (System/exit status))

(defn report-error!
  [e]
  (let [content (with-out-str (trace/print-cause-trace e))
        timestamp (str (java.time.Instant/now))]
    (spit log-file
          (str "[at " timestamp "]> " content)
          :append true)))

(defn remote-exec!
  [arguments options]
  (if-let [port (or (:port options) (read-port))]
    (try
      (send-msg!
       port
       (create-message! arguments))
      (catch Exception e
        (report-error! e)
        (println :error)))
    (println :disconnected)))

(defn -main
  "Sends a message through the client."
  [& args]
  (let [{:keys [arguments options exit-message ok?]} (validate-args args)]
    (cond
      (some? exit-message) (exit (if ok? 0 1) exit-message)
      (:dry-run options) (println "[running] >" (create-message! arguments))
      :else (remote-exec! arguments options))))

(when (= *file* (System/getProperty "babashka.file"))
  (apply -main *command-line-args*))

(comment
  (read-port)
  (validate-args ["git" "-p" "4520" "status"])
  (-main "-n" "-p" "32901" "keyvault" "list")
  (-main "keyvault" "list")
  (-main "-n" "git" "exit"))
