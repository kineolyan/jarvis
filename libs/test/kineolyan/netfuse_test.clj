(ns kineolyan.netfuse-test
  (:require [clojure.test :as t :refer [deftest is testing]]
            [kineolyan.netfuse :as m]))

(defn capture-ex
  [f & args]
  (let [ex (try
             (apply f args)
             nil
             (catch Exception e
               e))]
    (if (some? ex)
      ex
      (throw (AssertionError. "Not exception captured")))))

(defn fuse-ok?
  ([] (fuse-ok? m/*state*))
  ([state]
   (is (zero? (:attempts @state)))))

(defn throw-op
  []
  (throw (ex-info "oops" {:k :v})))

(deftest execution-with-network
  (binding [m/*state* (m/create-fuse)]

    (testing "successful operation"
      (is (= 3
             (m/run-with-fuse! + 1 2)))
      (fuse-ok?))

    (testing "throwing operation"
      (let [e (capture-ex m/run-with-fuse! throw-op)]
        (is (= "oops" (ex-message e)))
        (is (= {:k :v} (ex-data e)))
        (fuse-ok?)))))

(deftest execution-without-network
  (testing "successful operation"
    ;; Returns the result if the network is not used (no throw)
    (binding [m/*state* (m/create-fuse)
              m/*fake-broken-network* true]
      (is (= 24
             (m/run-with-fuse! * 2 3 4)))))

  (testing "throwing operation"
    ;; Records the failure
    (binding [m/*state* (m/create-fuse)
              m/*fake-broken-network* true]
      (let [{:keys [last-attempt]} @m/*state*
            e (capture-ex m/run-with-fuse! throw-op)
            new-state @m/*state*]
        (is (= "oops" (ex-message e)))
        (is (= {:k :v} (ex-data e)))
        (is (= 1 (:attempts new-state)))
        (is (< last-attempt (:last-attempt new-state))))))

  (testing "with blocked fuse"
    (binding [m/*state* (m/create-fuse (-> (m/create-fuse-state)
                                           (assoc :attempts 10)
                                           (assoc :last-attempt (System/nanoTime))
                                           (assoc :timeout 60000)))]
      (let [e (capture-ex m/run-with-fuse! throw-op)]
        (is (= {:attempt :rejected}
               (select-keys (ex-data e) [:attempt])))))))

(deftest execution-after-network-back
  (let [initial-state (-> (m/create-fuse-state)
                          (assoc :attempts 10)
                          (assoc :last-attempt 0))]
    (testing "successful operation"
      (binding [m/*state* (m/create-fuse initial-state)]
        (is (= 3
               (m/run-with-fuse! + 1 2)))
        (fuse-ok?)))
    (testing "throwing operation"
      (binding [m/*state* (m/create-fuse initial-state)]
        (let [e (capture-ex m/run-with-fuse! throw-op)]
          (is (= "oops" (ex-message e)))
          (is (= {:k :v} (ex-data e)))
          (fuse-ok?))))))

(deftest time-to-retry?
  (let [ms->ns #(.toNanos java.util.concurrent.TimeUnit/MILLISECONDS %)
        f (fn tested [timeout last-attempt current-time]
            (m/time-to-retry? {:timeout timeout
                               :last-attempt (ms->ns last-attempt)}
                              (ms->ns current-time)))]
    (testing "basic"
      (is (f 10 0 20))
      (is (not (f 10 0 1)))
      (is (not (f 10 0 9)))
      (is (not (f 10 0 10))))
    (testing "focusing on time units"
      (is (not (m/time-to-retry?
                {:timeout 1000 ; ms
                 :last-attempt 1000 ; ns
                 }
                2000 ; ns
                ))))))
