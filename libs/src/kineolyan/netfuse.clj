(ns kineolyan.netfuse
  (:require [babashka.http-client :as http]))

(defn create-fuse-state
  []
  {:attempts 0
   :last-attempt 0
   :timeout 1000
   :token :one})

(defn create-fuse
  ([]
   (create-fuse (create-fuse-state)))
  ([state]
   (atom state)))

(defonce ^:dynamic *state* (create-fuse))

(comment
  (reset! *state* (create-fuse-state)))

(def ^:dynamic *fake-broken-network* false)

(defn connection-up?!
  []
  (and
   (not *fake-broken-network*)
   (let [call (http/get "https://httpstat.us/200")]
     (= (:status call) 200))))

(comment
  (connection-up?!))

(defn -ns->ms
  [n]
  (.toMillis java.util.concurrent.TimeUnit/NANOSECONDS n))

(defn time-to-retry?
  [{:keys [last-attempt timeout]} current-time]
  (> (-ns->ms (- current-time last-attempt)) timeout))

(defn acquire-permit
  [state current-time]
  (cond
    (<= (:attempts state) 3)
    [state [::accept]]

    (and
     (:token state)
     (time-to-retry? state current-time))
    [(assoc state :token nil)
     [::retry 1]]

    :else
    [state [::reject]]))

(defn accept-connection?!
  [state]
  (loop []
    (let [s @state
          [next-state result] (acquire-permit s (System/nanoTime))]
      (if (compare-and-set! state s next-state)
        result
        (recur)))))

(defn record-connection-success!
  []
  (reset! *state* (create-fuse-state)))

(defn record-connection-error!
  [token]
  (if token
    (swap! *state*
           (fn [state]
             (-> state
                 (update :attempts inc)
                 (assoc :last-attempt (System/nanoTime))
                 (assoc :token :one)
                 ;; double timeout, max'ing at 1min (in ms)
                 (update :timeout * 2)
                 (update :timeout max (* 1000 60)))))
    (swap! *state*
           (fn [state]
             (-> state
                 (update :attempts inc)
                 (assoc :last-attempt (System/nanoTime)))))))

(defn execute!
  [token f args]
  (try
    (let [result (apply f args)]
      (record-connection-success!)
      result)
    (catch Exception e
      ;; Something went wrong in `f`, which involves network calls
      ;; Test manually connections are up
      ;; If so, enable the fuse; the error may have another source
      (if (connection-up?!)
        (record-connection-success!)
        (record-connection-error! token))
      (throw e))))

(defn create-network-exception
  []
  ;; TODO add some details about the current state of the fuse
  (ex-info "Network unavailable"
           {:attempt :rejected}))

(defn run-with-fuse!
  "Runs the provided function `f` with the passed arguments under the guard of the network fuse.
  In case of error, this function will check if network is available and possibly block future calls for a while."
  [f & args]
  (let [[r token] (accept-connection?! *state*)]
    (case r
      ::accept
      (execute! token f args)
      ::retry
      (execute! token f args)
      ::reject
      (throw (create-network-exception)))))

(defn reset-fuse!
  ([] (reset-fuse! *state*))
  ([state] (reset! state (create-fuse-state))))

(comment
  (deref *state*)
  (acquire-permit @*state* (System/nanoTime))
  (reset-fuse!)
  (System/nanoTime)
  (run-with-fuse! + 1 2)
  (run-with-fuse! #(throw (ex-info "oops" {})))
  (binding [*fake-broken-network* true]
    (run-with-fuse! * 2 3 4))
  (binding [*fake-broken-network* true]
    (run-with-fuse! #(throw (ex-info "oops broken network" {})))))
