(ns kineolyan.ssh
  (:require [babashka.fs :as fs]
            [clojure.string :as str]
            [kineolyan.shellby :as shb]))

;;; utility functions

(defn -trim-map
  [m]
  (into {} (filter (comp some? second) m)))

;;; ssh parser

(defn take-host-config-lines
  [lines]
  (when (seq lines)
    (let [host (first lines)
          [host-lines more-lines] (split-with #(not (str/starts-with? % "Host ")) (rest lines))]
      [(concat [host] host-lines)
       more-lines])))

(defn group-host-lines
  [lines]
  (loop [lines lines
         groups []]
    (if-let [[host-lines more-lines] (take-host-config-lines lines)]
      (recur more-lines (conj groups host-lines))
      groups)))

(defn get-value
  [pattern line]
  (when-let [[_ value] (re-find pattern line)]
    value))

(defn get-line-info
  [pattern lines]
  (->> lines
       (map #(get-value pattern %))
       (filter some?)
       (first)))

(defn parse-host-lines
  [lines]
  (let [hostname (get-value #"^Host (.+)" (first lines))
        username (get-line-info #"^\s+User (.+)" lines)
        id-file (some->> lines
                         (get-line-info #"^\s+IdentityFile (.+)")
                         (fs/expand-home))
        config {:hostname hostname
                :username username
                :identity id-file}]
    (-trim-map config)))

(def default-identity-files
  (map
   #(fs/path (fs/home) ".ssh" %)
   ["id_dsa"
    "id_ecdsa"
    "id_ecdsa_sk"
    "id_ed25519"
    "id_ed25519_sk"
    "id_rsa"]))

(defn create-default-host-config
  []
  (-trim-map
   {:hostname :wildcard
    :username nil
    :identity (->> default-identity-files
                   (filter fs/exists?)
                   first)}))

(def standard-config-path (fs/path (fs/home) ".ssh" "config"))

(defn read-config
  "Reads the SSH configuration.
   If `f` is provided, read the file at the given path. Otherwise, it reads $HOME/.ssh/config.
  Returns the list of configured hosts with the following information `{:keys [hostname username identity]}`.
  The configuration ships a special value, whose `:hostname` is `:wildcard`, containing the host config matching every ssh connection not listed in the config."
  ([] (read-config standard-config-path))
  ([f]
   (if-not (fs/exists? f)
     [(create-default-host-config)]
     (let [content (slurp (str f))
           lines (->> content
                      (str/split-lines)
                      (filter (complement str/blank?)))
           groups (group-host-lines lines)]
       (into [(create-default-host-config)]
             (map parse-host-lines groups))))))

(comment
  (def config (read-config)))

;;; ssh helper functions

(defn list-ssh-agent-sockets
  []
  (let [root-dirs [(System/getenv "XDG_RUNTIME_DIR")
                   (fs/temp-dir)]
        ssh-session-dirs (filter fs/directory? (fs/list-dirs root-dirs "ssh-*"))
        ssh-sockets (->> ssh-session-dirs
                         (map #(fs/match % "regex:agent\\..+"))
                         (flatten)
                         #_(filter fs/regular-file?))]
    ssh-sockets))

(defn resolve-ssh-agent-socket
  []
  (if-let [env-ssh (System/getenv "SSH_AUTH_SOCK")]
    (fs/path env-ssh)
    (let [[socket & more] (list-ssh-agent-sockets)]
      (when (and socket (empty? more))
        socket))))

(defn create-env-for-ssh
  []
  (when-let [socket (resolve-ssh-agent-socket)]
    {"SSH_AUTH_SOCK" (str socket)}))

(comment
  (list-ssh-agent-sockets)
  (resolve-ssh-agent-socket)
  (create-env-for-ssh))

(defn $has-passphrase?
  [f]
  {:args ["ssh-keygen" "-yP" "" "-f" (str f)]
   :f #(not= (:exit %) 0)})

(defn -read-signature-from-output
  [line]
  ; A line looks like "4096 SHA256:KYkBr2d8WEd... kineolyan@gmail.com (RSA)"
  (-> line
      (str/split #"\s+")
      (second)))

(defn $read-signature
  [f]
  {:args ["ssh-keygen" "-lf" (str f)]
   :f #(-> % shb/check :out str/trim-newline -read-signature-from-output)})

(defn $read-loaded-keys
  "List all loaded keys in the current ssh-agent session."
  []
  {:args '(ssh-add -l)
   :env (create-env-for-ssh)
   :f #(case (:exit %)
         0 (->> %
                :out
                str/split-lines
                (map -read-signature-from-output)
                vec)
         1 []
         ; Exit code when ssh-add cannot connect to the agent
         ; Consider it as not having any key loaded
         2 []
         :else (shb/check %))})

(defn $loaded-key?
  "Tests if the private key belongs to the loaded keys."
  [f]
  {:a-deps [[$read-loaded-keys]
            [$read-signature f]]
   :f (fn [[ks k]] (boolean (some #{k} ks)))})

(defn $require-passphrase?
  "Tests if a given host, described by its definition, requires a passphrase to be entered to be
  used with remote operations like fetch, push."
  [host-def]
  (if-let [identity-file (:identity host-def)]
    {:a-deps [[$has-passphrase? identity-file]
              [$loaded-key? identity-file]]
     :f (fn [[passphrase? loaded?]] (and passphrase? (not loaded?)))}
    {:just false}))

(comment
  (def extended-config
    (->> config
         (map #(assoc %
                      :protected (some->> % :identity (shb/sh $has-passphrase?))
                      :sign (some->> % :identity (shb/sh $read-signature))))
         (map -trim-map)
         (map #(select-keys % [:hostname :identity :protected :sign]))))

  (require '[clojure.pprint])
  (clojure.pprint/print-table extended-config)

  (map #(some->> % :identity (shb/sh $has-passphrase?)) config)
  (doseq [{id :identity} config]
    (when id
      (println (shb/sh $read-signature id))))
  (map #(when (and (contains? % :identity)
                   (shb/sh $has-passphrase? (:identity %)))
          (shb/sh $loaded-key? (:identity %)))
       config)
  (map #(shb/sh $require-passphrase? %)
       config)

  (shb/sh $read-loaded-keys)

  (some->> config first :identity (shb/sh $read-signature)))
