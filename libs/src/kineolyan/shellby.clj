(ns kineolyan.shellby
  (:require
   [clojure.core.match :refer [match]]
   [clojure.java.shell :as shell]
   [babashka.fs :as fs]
   [missionary.core :as m]
   [kineolyan.stm :as stm]))

;;; Protocol definition

(defprotocol Executor
  (run-command [this  payload])
  (run-async-commands [this payload])
  (run-sync-commands [this payload])
  (return-value [this value]))

(defn -async-dependencies?
  [dependencies]
  (some
   #(% dependencies)
   [map?]))

(defn -format-dependencies
  [dependencies]
  (let [defs (cond
               (map? dependencies)
               {:commands (vals dependencies)
                :builder #(zipmap (keys dependencies) %)}
               (or (vector? dependencies)
                   (seq? dependencies))
               {:commands dependencies
                :builder vec})
        create-dep-action (fn [[f & args]] (apply f args))]
    (update defs :commands #(map create-dep-action %))))

(defn -run-with-deps
  [executor {:keys [deps f]} exec]
  (let [{:keys [commands builder]} (-format-dependencies deps)
        finisher #(f (builder %))]
    (exec executor {:commands commands
                    :f finisher})))

(defn exec
  [executor payload]
  (match [payload]
    [{:args args}]
    (run-command
     executor
      ; ensure that arguments are correctly formatted as strings
     (assoc payload :args (map str args)))
    [{:deps _ :async false}]
    (-run-with-deps executor payload run-sync-commands)
    [{:deps _ :async true}]
    (-run-with-deps executor payload run-async-commands)
    [({:deps deps} :only [:deps :f])]
    (-run-with-deps
     executor
     payload
     (if (-async-dependencies? deps)
       run-sync-commands
       run-async-commands))
    [{:just value}]
    (return-value executor value)
    [{:s-deps deps}]
    (exec executor (-> payload
                       (dissoc :s-deps)
                       (assoc :deps deps :async false)))
    [{:a-deps deps}]
    (exec executor (-> payload
                       (dissoc :a-deps)
                       (assoc :deps deps :async true)))))

;;; Shell executor, blocking, using clojure.java.shell/sh

(defn sh-call
  "Executor to use to perform calls to external programs.
   This is a function receiving `{:args :dir :env}` and returns `{:exit :out :err}`"
  [{:keys [args dir env f] :as call}]
  (let [sh-args (concat args
                        (when dir [:dir dir])
                        (when env [:env env]))
        result (apply shell/sh sh-args)
        payload (merge (dissoc call :f) result)]
    (f payload)))

(def sh-executor
  "Executor using sync calls via `clojure.java.shell/sh`.
  This instance is quite useful to easily test commands."
  (reify Executor
    (run-command [_  payload]
      (sh-call payload))
    (run-async-commands [this payload]
      (run-sync-commands this payload))
    (run-sync-commands [this {:keys [commands f]}]
      (let [results (mapv #(exec this %) commands)]
        (f results)))
    (return-value [_ value]
      value)))

(comment
  (exec sh-executor {:args ["ls" "-l"]
                     :f identity}))

;;; shell-out function on top of `missionary`

(defn -create-process
  [args {:keys [env dir] :or {env {}}}]
  (let [builder! (ProcessBuilder. args)]
    (when dir (.directory builder! (fs/file dir)))
    (doseq [[k v] env]
      (if v
        (.put (.environment builder!) k v)
        (.remove (.environment builder!) k)))
    (.start builder!)))

(defn fn->biconsumer
  [f]
  (reify
    java.util.function.BiConsumer
    (accept [_this a b] (f a b))))

(defn -complete-process
  [{:keys [status success fail process error out-consumer err-consumer]}]
  (case (stm/transition! status :completed)
    :completed
    (let [result {:exit (.exitValue process)
                  :out (out-consumer (.getInputStream process))
                  :err (err-consumer (.getErrorStream process))}]
      (if error
        (fail (merge result {:error error}))
        (success result)))
    :aborted
    (fail nil)))

(defn -build-completion-payload
  [{:keys [success fail outs]}]
  (let [out-fn (if (= outs :str) slurp identity)]
    {:success success
     :fail fail
     :status (stm/satom
              [[:started :completed] [:started :aborted]]
              :started)
     :out-consumer out-fn
     :err-consumer out-fn}))

(defn -attach-to-process!
  [process completion-payload]
  (-> process
      (.onExit)
      (.whenComplete
       (fn->biconsumer
        (fn [p e]
          (-complete-process (merge completion-payload {:process p :error e})))))))

(defn -create-cancel-fn
  [process completion-payload]
  (fn []
    (stm/transition! (completion-payload :status) :aborted)
    (.destroy process)))

(defn missionary-sh
  [args options]
  (fn [success fail]
    (let [process (-create-process args options)
          completion-payload (-build-completion-payload (merge options {:success success
                                                                        :fail fail}))]
      (-attach-to-process! process completion-payload)
      (-create-cancel-fn process completion-payload))))

(comment
  ; working
  (m/? (missionary-sh ["ls" "-l"] {:dir "/home/oliv/tmp" :outs :str}))
  (m/? (missionary-sh ["bash" "-c" "echo -n $CUSTOM_VAR"]
                      {:outs :str
                       :env {"CUSTOM_VAR" "HELLO"}}))
  (m/? (missionary-sh ["bash" "-c" "echo start; sleep 1; echo end"] {:outs :str}))
  ; with cancellation
  (m/? (missionary-sh ["bash" "-c" "echo start; sleep 2; echo end"] {:outs :str}))
  (m/? (m/timeout
        (missionary-sh ["bash" "-c" "echo start; sleep 2; echo end"] {:outs :str})
        100
        :aborted)))

;;; Executor with missionary

(defn missionary-call
  [{:keys [args dir env f] :as call}]
  ;; TODO let's start by always returning :out/:err as strings
  ;; Otherwise, we will have to create a similar behaviour whatever the executor
  (let [options (into {:outs :str} (concat []
                                           (when dir [[:dir dir]])
                                           (when env [[:env env]])))]
    (m/sp
     (let [result (m/? (missionary-sh args options))
           payload (merge (dissoc call :f) result)]
       (f payload)))))

(def missionary-executor
  (reify Executor
    (run-command [_  payload]
      (missionary-call payload))
    (run-async-commands [this {:keys [commands f]}]
      (m/sp
       (let [actions (map #(exec this %) commands)
             results (m/? (apply m/join (concat [vector] actions)))]
         (f results))))
    (run-sync-commands [this {:keys [commands f]}]
      (m/sp
       (loop [[dep & deps] commands
              results []]
         (if dep
           (recur
            deps
            (conj results (m/? (exec this dep))))
           (f results)))))
    (return-value [_ value]
      (m/sp value))))

(comment
  (defn $sleep
    [n]
    {:args ["bash" "-c" (format "sleep %d" n)]
     :f (constantly [:slept n])})
  (exec sh-executor {:args ["ls" "-l"]
                     :f identity})
  (m/? (missionary-call {:args ["ls" "-l"]
                         :f identity}))
  (m/?
   (exec missionary-executor {:args ["ls" "-l"]
                              :f identity}))
  (m/? (exec missionary-executor ($sleep 2)))
  (defn validate-results
    [results]
    (match [results]
      [[[:slept a] [:slept b]]] (+ a b)
      :else [:oops results]))
  (time (m/?
         (exec missionary-executor
               {:deps [[$sleep 2]
                       [$sleep 3]]
                :async true
                :f validate-results})))
  (time (m/?
         (exec missionary-executor
               {:deps [[$sleep 2]
                       [$sleep 3]]
                :async false
                :f validate-results})))
  (exec sh-executor {:s-deps [[$sleep 2]
                              [$sleep 3]]
                     :f validate-results}))

;;; public API

(defmacro m-sh?
  "Simple macro creating a shellby call with missionary executor and evaluating it with ?.
  A macro is required to be detected by missionary in the calling stack."
  [& [f & args]]
  `(let [cmd# (~f ~@args)]
     (m/? (exec missionary-executor cmd#))))

(defn sh
  "Calls a shellby operation with args using the shell executor.
  This blocks until the shell operation completes."
  [& [f & args]]
  (exec sh-executor (apply f args)))

(comment
  (exec sh-executor {:args '(ls -l)
                     :f identity})
  (sh (fn [& _] (hash-map :args '(ls l) :f identity))))

(comment
  (macroexpand-1 '(m-sh? $sleep 1)))

;;; utility functions to process results

(defn check
  "Checks that the exit code of the function call zero, convention for success.
  Returns the call result, without filtering.
  This throws otherwise."
  [result]
  (when-not (zero? (:exit result))
    (throw (ex-info "Error in command execution" result)))
  result)
