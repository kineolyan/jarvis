(ns kineolyan.stm)

;;; state machines with atom

(defn ->transition-fn
  [f]
  (cond
    (fn? f)
    f ;; maybe check the number of args
    (seq f)
    (let [transitions (set f)]
      (fn [previous-state new-state]
        (if (contains? transitions [previous-state new-state])
          new-state
          previous-state)))
    :else
    (throw (IllegalArgumentException. (str "Unsupported transition description" f)))))

(defn satom
  ([f]
   (satom f nil))
  ([f initial-value]
   (atom initial-value :meta {:transitions (->transition-fn f)})))

(defn transition!
  [a state]
  (let [f (-> a meta :transitions)]
    (swap! a f state)))

(comment
  (def a (satom
          [[:started :stopped]
           [:started :paused]
           [:paused :started]]
          :started))
  (transition! a :paused)
  (transition! a :stopped)
  (transition! a :started)
  (transition! a :stopped)
  (meta a))
